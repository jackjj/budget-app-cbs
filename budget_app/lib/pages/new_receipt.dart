import 'dart:async';

import 'package:budget_onderzoek/pages/search.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/quick_search.dart';
import 'package:budget_onderzoek/util/string_matching_shops.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

double x;
double y;
double f;

class ReceiptDetailsPage extends StatelessWidget {

   Future<void> loadDatabases() async{
    QuickSearch.loadQuickSearchTable();
    StringMatchingShops.loadShopTable();
  }
  @override
  Widget build(BuildContext context) {
    loadDatabases();
    // textController.text = NewTransactionModel.of(context).receiptPrice;
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));

    return WillPopScope(
      onWillPop: () {
        if (NewTransactionModel.of(context).modifyTransactionID != null) {
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: ColorPallet.lightBlue,
          ));
        }

        Navigator.of(context).pop();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPallet.pink,
          title: Text(translate.addInfo , style: TextStyle(fontSize: 24*f)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              StoreSelectionList(),
              SizedBox(height: 10*y),
              Image.asset(
                NewTransactionModel.of(context).receiptLocation,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class StoreSelectionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 12*y),
      width: 400*x,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0*x),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: Offset(5.0*y, 1.0*x),
              blurRadius: 2.0*x,
              spreadRadius: 3.0*x)
        ],
      ),
      child: ScopedModelDescendant<NewTransactionModel>(
        builder: (context, child, model) => Column(
              children: <Widget>[
                SizedBox(height: 20*y, width: MediaQuery.of(context).size.width),
                DateInput(),
                StoreInput(),
                PriceInput(),
                SizedBox(height: 10*y),
                ScopedModelDescendant<NewTransactionModel>(
                  builder: (context, child, model) => Container(
                        width: 130*x,
                        height: 35*y,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0*x),
                          ),
                          color: model.receiptComplete
                              ? ColorPallet.darkBlue
                              : ColorPallet.midGray,
                          child: Text(translate.add,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 17.0*f)),
                          onPressed: () async {
                            model.addNewTransAction();
                            SystemChrome.setSystemUIOverlayStyle(
                              SystemUiOverlayStyle(
                                  statusBarColor: ColorPallet.lightBlue),
                            );
                            if (model.modifyTransactionID == null) {
                              Navigator.of(context).pop();
                            }
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                ),
              ],
            ),
      ),
    );
  }
}

class StoreInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0*x, vertical: 8*y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border: Border.all(color: ColorPallet.lightGray, width: 1.7*x),
                borderRadius: BorderRadius.circular(12.0*x)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0*x, vertical:10.0*y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SearchWidget(false),
                    ),
                  );
                  if (result != null) {
                    newTransaction.store = result;
                    newTransaction.isReceiptInfoComplete();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.store,
                            color: ColorPallet.darkBlue, size: 27*x),
                        SizedBox(width: 10.0*x),
                        Container(
                          width: 305*x,
                          child: Text(
                              newTransaction.store != ""
                                  ? newTransaction.store
                                  : translate.enterShop + " ...",
                              style: TextStyle(
                                  color: newTransaction.store != ""
                                      ? ColorPallet.darkBlue
                                      : ColorPallet.midGray,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18*f),
                              overflow: TextOverflow.ellipsis),
                        )
                      ],
                    ),
                    newTransaction.storeType == ""
                        ? Container()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: 5*y),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.category,
                                      color: ColorPallet.darkBlue, size: 27*x),
                                  SizedBox(width: 10.0*x),
                                  Container(
                                    width: 305*x,
                                    child: Text(newTransaction.storeType,
                                        style: TextStyle(
                                            color: ColorPallet.darkBlue
                                                .withOpacity(0.7),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18*f),
                                        overflow: TextOverflow.ellipsis),
                                  )
                                ],
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48*x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0*x),
            color: Colors.white,
            child: Text(
              translate.shop,
              style: TextStyle(
                  color: ColorPallet.darkBlue, fontWeight: FontWeight.w700, fontSize: 14*f),
            ),
          ),
        ),
      ],
    );
  }
}

class DateInput extends StatefulWidget {
  @override
  _DateInputState createState() => _DateInputState();
}

class _DateInputState extends State<DateInput> {
  NewTransactionModel newTransaction;
  String hintText = translate.today;
  DateTime initialDate =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  Future<Null> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(2101),
    );
    picked = DateTime(picked.year, picked.month, picked.day);
    if (picked != null) {
      newTransaction.date = DateFormat('yyyy-MM-dd').format(picked);
      newTransaction.notifyListeners();
    }
  }

  String getDateString(String date) {
    DateTime today =
        DateTime(initialDate.year, initialDate.month, initialDate.day);
    DateTime yesterday =
        DateTime(initialDate.year, initialDate.month, initialDate.day - 1);
    DateTime tomorrow =
        DateTime(initialDate.year, initialDate.month, initialDate.day + 1);

    if (date == DateFormat('yyyy-MM-dd').format(today)) {
      return translate.today;
    } else if (date == DateFormat('yyyy-MM-dd').format(yesterday)) {
      return translate.yesterday;
    } else if (date == DateFormat('yyyy-MM-dd').format(tomorrow)) {
      return translate.tomorrow;
    } else {
      return DateFormat('EEEE, d MMMM', translate.languagePreference)
          .format(DateTime.parse(date));
    }
  }

  @override
  Widget build(BuildContext context) {
    newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0*x, vertical:8.0*y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border: Border.all(color: ColorPallet.lightGray, width: 1.7*x),
                borderRadius: BorderRadius.circular(12.0*x)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0*x, vertical: 10.0*y),
              child: InkWell(
                onTap: () async {
                  _selectDate(context);
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.today, color: ColorPallet.darkBlue, size: 27*x),
                    SizedBox(width: 10.0*x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          getDateString(model.date),
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18*f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48*x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0*x),
            color: Colors.white,
            child: Text(translate.date,
                style: TextStyle(
                    color: ColorPallet.darkBlue, fontWeight: FontWeight.w700, fontSize: 14*f)),
          ),
        ),
      ],
    );
  }
}

class PriceInput extends StatefulWidget {
  @override
  _PriceInputState createState() => _PriceInputState();
}

class TextEditingControllerWorkaroud extends TextEditingController {
  void setTextAndPosition(String newText, {int caretPosition}) {
    int offset = caretPosition != null ? caretPosition : newText.length;
    value = value.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: offset),
        composing: TextRange.empty);
  }
}

class _PriceInputState extends State<PriceInput> {
  String price = "";
  TextEditingControllerWorkaroud textController =
      TextEditingControllerWorkaroud();
  void checkUserInput(String newPrice, BuildContext context) {
    //newPrice = UserInput.valueSanitizer(newPrice);
    if (!UserInput.valueValidator(newPrice)) {
      Toast.show(translate.invalidAmount, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      textController.setTextAndPosition(price);
    } else {
      if (newPrice == ".") {
        price = "0.";
      } else {
        price = newPrice;
      }

      NewTransactionModel.of(context).receiptPrice = price;
      NewTransactionModel.of(context).isReceiptInfoComplete();
    }
  }

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(0.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border: Border.all(color: ColorPallet.lightGray, width: 1.7 *x),
                borderRadius: BorderRadius.circular(12.0*x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0*x),
                Icon(Icons.local_offer, color: ColorPallet.darkBlue, size: 27*x),
                // SizedBox(width: 3.0),
                Container(
                  alignment: Alignment.topLeft,
                  width: 200*x,
                  height: 40*y,
                  margin: EdgeInsets.only(bottom: 3.5*x, top: 6*y),
                  child: TextField(
                    controller: textController,
                    style: TextStyle(
                        color: ColorPallet.darkBlue,
                        fontWeight: FontWeight.w500,
                        fontSize: 18*f),
                    autocorrect: false,
                    keyboardType: TextInputType.number,
                    onChanged: (newPrice) {
                      setState(() {
                        checkUserInput(newPrice, context);
                      });
                    },
                    onSubmitted: (value) {
                      if (NewTransactionModel.of(context).receiptPrice != "") {
                        price = double.parse(price).toStringAsFixed(2);
                        textController.text = price;
                        NewTransactionModel.of(context).receiptPrice = price;
                        NewTransactionModel.of(context).isReceiptInfoComplete();
                      }
                    },
                    decoration: InputDecoration(
                      hintText: NewTransactionModel.of(context).receiptPrice !=
                              ""
                          ? double.parse(
                                  NewTransactionModel.of(context).receiptPrice)
                              .toStringAsFixed(2)
                          : translate.enterTotalPrice + " ...",
                      hintStyle: TextStyle(
                          color: ColorPallet.midGray,
                          fontWeight: FontWeight.w500,
                          fontSize: 18*f),
                      filled: true,
                      fillColor: Colors.transparent,
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48*x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0),
            color: Colors.white,
            child: Text(
              translate.totalPrice,
              style: TextStyle(
                fontSize: 14*f,
                color: ColorPallet.darkBlue,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
