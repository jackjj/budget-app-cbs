import 'package:budget_onderzoek/util/translate.dart';
import 'package:budget_onderzoek/widgets/calendar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/text_strings.dart';

double x;
double y;
double f;

class OverviewPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OverviewPageState();
  }
}

class _OverviewPageState extends State<OverviewPage> {
  int _calendarYear;
  int _calendarMonth;
  bool _informationPopup = false;
  String _displayUser = "Tom";
  double initial;
  double distance;

  @override
  void initState() {
    super.initState();
    _calendarYear = DateTime.now().year;
    _calendarMonth = DateTime.now().month;
  }

  void setUser(String user) {
    setState(() {
      _displayUser = user;
    });
  }

  void currentCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = DateTime.now().year;
        _calendarMonth = DateTime.now().month;
      });
    }
  }

  void _nextCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear =
            _calendarMonth == 12 ? _calendarYear + 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 12 ? 1 : _calendarMonth + 1;
      });
    }
  }

  void _previousCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = _calendarMonth == 1 ? _calendarYear - 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 1 ? 12 : _calendarMonth - 1;
      });
    }
  }

  String getMonthString(int monthInt) {
    String monthString = DateFormat.MMMM(translate.languagePreference)
        .format(DateTime(_calendarYear, _calendarMonth, 1));
    return (monthString[0].toUpperCase() + monthString.substring(1));
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          _TopBarWidget(
              _nextCalendarMonth,
              _previousCalendarMonth,
              getMonthString(_calendarMonth),
              _informationPopup,
              setUser,
              _displayUser,
              currentCalendarMonth,
              _calendarMonth,
              _calendarYear),
          _MidSectionWidget(_nextCalendarMonth, _previousCalendarMonth,
              _calendarMonth, _calendarYear),
        ],
      ),
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  _TopBarWidget(
      this.nextCalendarMonth,
      this.previousCalendarMonth,
      this.displayMonth,
      this.informationPopup,
      this.setUser,
      this.displayUser,
      this.currentCalendarMonth,
      this._calendarMonth,
      this._calendarYear);

  final void Function() nextCalendarMonth;
  final void Function() previousCalendarMonth;
  final void Function(String user) setUser;
  final void Function() currentCalendarMonth;
  final String displayMonth;
  final bool informationPopup;
  final String displayUser;
  final int _calendarMonth;
  final int _calendarYear;
  final List<String> users = <String>["Tom", "Karin", "Kim", "Ron", "Dexter"];

  bool isCurrentMonth() {
    if (_calendarYear == DateTime.now().year &&
        _calendarMonth == DateTime.now().month) {
      return true;
    }
    return false;
  }

  showTopMenu(BuildContext context, Function(String user) setUser) async {
    var selectedValue = await showMenu(
        context: context,
        position: RelativeRect.fromLTRB(60 * x, 40 * y, 50 * x, 50 * y),
        items: users.map((String user) {
          return PopupMenuItem(
            value: user,
            child: Text(
              user,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontSize: 18.0 * f,
                  fontWeight: FontWeight.w500),
            ),
          );
        }).toList());
    if (selectedValue != null) {
      setUser(selectedValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    Color interactionColor =
        informationPopup ? ColorPallet.hiddenBlue : Colors.white;
    return Stack(
      children: <Widget>[
        InkWell(
          child: Image(
              image: AssetImage("images/topbar.png"), fit: BoxFit.fitWidth),
        ),
        Positioned(
          right: 10.0 * x,
          top: 10.0 * y,
          child: InkWell(
            onTap: () {
              if (!informationPopup) {
                showTopMenu(context, setUser);
              }
            },
            child: Row(
              children: <Widget>[
                Text(
                  displayUser,
                  style: TextStyle(
                      color: interactionColor,
                      fontSize: 20.0 * f,
                      fontWeight: FontWeight.w700),
                ),
                Icon(Icons.keyboard_arrow_down,
                    color: interactionColor, size: 25.0 * x),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 17 * y,
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    InkWell(
                      child: Icon(Icons.keyboard_arrow_left,
                          color: interactionColor, size: 29.0 * x),
                      onTap: previousCalendarMonth,
                    ),
                    SizedBox(width: 10.0 * x),
                    Container(
                      width: 120.0 * x,
                      child: Center(
                        child: Text(
                          displayMonth,
                          style: TextStyle(
                              color: interactionColor,
                              fontSize: 20.0 * f,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0 * x),
                    InkWell(
                      child: Icon(Icons.keyboard_arrow_right,
                          color: interactionColor, size: 29.0 * x),
                      onTap: nextCalendarMonth,
                    ),
                  ],
                ),
                SizedBox(width: 62.0 * x),
                Container(width: 26.5 * x),
                // isCurrentMonth()
                //     ? Container(width: 26.0 * x)
                //     : InkWell(
                //         child: Icon(Icons.refresh,
                //             color: interactionColor, size: 26 * x),
                //         onTap: () {
                //           currentCalendarMonth();
                //         }),
                SizedBox(width: 20.0 * x),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class _MidSectionWidget extends StatefulWidget {
  _MidSectionWidget(this._nextCalendarMonth, this._previousCalendarMonth,
      this._calendarMonth, this._calendarYear);
  final Function() _nextCalendarMonth;
  final Function() _previousCalendarMonth;
  final int _calendarMonth;
  final int _calendarYear;

  @override
  State<StatefulWidget> createState() {
    return _MidSectionWidgetState();
  }
}

class _MidSectionWidgetState extends State<_MidSectionWidget> {
  bool _informationPopup = false;

  void showInformationPopup() {
    setState(() {
      _informationPopup = true;
    });
  }

  void hideInformationPopup() {
    setState(() {
      _informationPopup = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(bottom: 6.0 * y),
        child: _informationPopup
            ? _InformationPopup(hideInformationPopup)
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _CalendarWidget(
                      widget._nextCalendarMonth,
                      widget._previousCalendarMonth,
                      widget._calendarYear,
                      widget._calendarMonth),
                  Padding(
                    padding:  EdgeInsets.only(top: 3.0*y),
                    child: _ProgressWidget(showInformationPopup),
                  ),
                ],
              ),
      ),
    );
  }
}

class _CalendarWidget extends StatelessWidget {
  _CalendarWidget(this._nextCalendarMonth, this._previousCalendarMonth,
      this._calendarYear, this._calendarMonth);

  final Function() _nextCalendarMonth;
  final Function() _previousCalendarMonth;
  final int _calendarMonth;
  final int _calendarYear;

  @override
  Widget build(BuildContext context) {
    double initialSwipe;
    double distanceSwiped;
    return GestureDetector(
        child: CalanderWidget(_calendarYear, _calendarMonth),
        onPanStart: (DragStartDetails details) {
          initialSwipe = details.globalPosition.dx;
        },
        onPanUpdate: (DragUpdateDetails details) {
          distanceSwiped = details.globalPosition.dx - initialSwipe;
        },
        onPanEnd: (DragEndDetails details) {
          initialSwipe = 0.0;
          if (distanceSwiped < 50) {
            _nextCalendarMonth();
          }
          if (distanceSwiped > 50) {
            _previousCalendarMonth();
          }
        });
  }
}

class _ProgressWidget extends StatelessWidget {
  final void Function() showInformationPopup;

  _ProgressWidget(this.showInformationPopup);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: showInformationPopup,
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 9.0 * x),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: ColorPallet.veryLightGray,
                    offset: new Offset(1.0 * x, 1.0 * x),
                    blurRadius: 2.0 * x,
                    spreadRadius: 3.0 * x)
              ],
            ),
            child: Container(
              width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
              height: 132.0 * y,
              child: Row(
                children: <Widget>[
                  SizedBox(width: 25 * x),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0 * y,
                      ),
                      Text(
                        translate.daysCompleted + ":",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 14.0 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(height: 12.0 * y),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: 15.0 * y,
                            width: 15.0 * x,
                            decoration: BoxDecoration(
                              color: ColorPallet.lightGreen,
                              boxShadow: [
                                BoxShadow(
                                    color: ColorPallet.veryLightGray,
                                    offset: Offset(1.0 * x, 1.0 * y),
                                    blurRadius: 1.0 * x,
                                    spreadRadius: 1.0 * x)
                              ],
                            ),
                          ),
                          SizedBox(width: 10.0 * x),
                          Text(
                            "3" + " " + translate.daysCompleted,
                            style: TextStyle(
                                fontSize: 14.0 * f,
                                color: ColorPallet.darkBlue,
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                      SizedBox(height: 8.0 * y),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: 15.0 * y,
                            width: 15.0 * x,
                            decoration: BoxDecoration(
                              color: ColorPallet.orange,
                              boxShadow: [
                                BoxShadow(
                                    color: ColorPallet.veryLightGray,
                                    offset: Offset(1.0 * x, 1.0 * y),
                                    blurRadius: 1.0 * x,
                                    spreadRadius: 1.0 * x)
                              ],
                            ),
                          ),
                          SizedBox(width: 10.0 * x),
                          Text(
                            "2" + " " + translate.daysMissing,
                            style: TextStyle(
                                fontSize: 14.0 * f,
                                color: ColorPallet.darkBlue,
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                      SizedBox(height: 8.0 * y),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: 15.0 * y,
                            width: 15.0 * x,
                            decoration: BoxDecoration(
                              color: ColorPallet.veryLightBlue,
                              boxShadow: [
                                BoxShadow(
                                    color: ColorPallet.veryLightGray,
                                    offset: Offset(1.0 * x, 1.0 * y),
                                    blurRadius: 1.0 * x,
                                    spreadRadius: 1.0 * x)
                              ],
                            ),
                          ),
                          SizedBox(width: 10.0 * x),
                          Text(
                            "8" + " " + translate.daysRemaining,
                            style: TextStyle(
                                fontSize: 14.0 * f,
                                color: ColorPallet.darkBlue,
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 9.0 * x),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: ColorPallet.veryLightGray,
                    offset: new Offset(1.0 * x, 1.0 * x),
                    blurRadius: 2.0 * x,
                    spreadRadius: 3.0 * x)
              ],
            ),
            child: Container(
              width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
              height: 132.0 * y,
              child: Row(
                children: <Widget>[
                  SizedBox(width: 35 * x),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 30 * y, width: 128 * x),
                      Text(
                        "3/20 euro",
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w700,
                            fontSize: 20 * f),
                      ),
                      SizedBox(height: 5 * y),
                      Text(translate.earnedReward,
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w700,
                              fontSize: 13 * f)),
                      SizedBox(height: 25 * y),
                      Row(
                        children: <Widget>[
                          Text(translate.moreInfo,
                              style: TextStyle(
                                  color: ColorPallet.midGray,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 13 * f)),
                          SizedBox(width: 7 * x),
                          Icon(Icons.info,
                              color: ColorPallet.midGray, size: 16 * x),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _InformationPopup extends StatelessWidget {
  final void Function() hideInformationPopup;

  _InformationPopup(this.hideInformationPopup);

  _launchURL() async {
    const url = 'https://www.cbs.nl';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400.0 * y,
        margin: EdgeInsets.symmetric(horizontal: 7.0),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0 * y),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 8,
                      child: Center(
                        child: Text(translate.infoTitle,
                            style: TextStyle(
                                color: ColorPallet.darkBlue,
                                fontSize: 18.0 * f,
                                fontWeight: FontWeight.w700)),
                      )),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: InkWell(
                        child: Icon(
                          Icons.close,
                          color: ColorPallet.darkBlue,
                          size: 27.0 * x,
                        ),
                        onTap: hideInformationPopup,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 10,
              child: Scrollbar(
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 20.0 * x, vertical: 5.0 * y),
                  child: SingleChildScrollView(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: translate.infoBody,
                          style: TextStyle(
                              color: ColorPallet.darkBlue, fontSize: 16.0 * f),
                        ),
                        TextSpan(
                            text: translate.infoForMoreInfo + " ",
                            style: TextStyle(
                                color: ColorPallet.darkBlue,
                                fontSize: 16.0 * f)),
                        TextSpan(
                          text: translate.infoWebsiteLink,
                          style: TextStyle(
                              color: ColorPallet.lightBlue, fontSize: 16.0 * f),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              _launchURL();
                            },
                        ),
                        TextSpan(text: "\n"),
                      ]),
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
