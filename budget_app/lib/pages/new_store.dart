import 'dart:async';
import 'package:budget_onderzoek/pages/new_products.dart';
import 'package:budget_onderzoek/pages/search.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/quick_search.dart';
import 'package:budget_onderzoek/util/slide_page_transition.dart';
import 'package:budget_onderzoek/util/string_matching_shops.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

double x;
double y;
double f;

class SelectStorePage extends StatefulWidget {
  @override
  _SelectStorePageState createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return _SelectStorePageState();
  }
}

class _SelectStorePageState extends State<SelectStorePage> {

  Future<void> loadDatabases() async{
    QuickSearch.loadQuickSearchTable();
    StringMatchingShops.loadShopTable();
  }

  @override
  Widget build(BuildContext context) {
    loadDatabases();
    
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return WillPopScope(
      onWillPop: () {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: ColorPallet.lightBlue,
        ));
        Navigator.of(context).pop();
      },
      child: Theme(
        data: ThemeData(
          primaryColor: ColorPallet.pink,
          accentColor: ColorPallet.pink,
        ),
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              translate.selectShop,
              style: TextStyle(color: Colors.white, fontSize: 23 * f),
            ),
            backgroundColor: ColorPallet.pink,
          ),
          body: Column(
            children: <Widget>[
              SizedBox(
                height: 7 * y,
                width: MediaQuery.of(context).size.width,
              ),
              StoreSelectionList(),
              Expanded(child: Container()),
              ScopedModelDescendant<NewTransactionModel>(
                builder: (context, child, model) => Container(
                      width: 171 * x,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0 * x),
                        ),
                        color: model.storeInfoComplete
                            ? ColorPallet.darkBlue
                            : ColorPallet.midGray,
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              height: 41 * y,
                              width: 11 * x,
                            ),
                            Text(translate.products,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 17.0 * f)),
                            SizedBox(width: 15.0 * x),
                            Icon(Icons.arrow_forward,
                                color: Colors.white, size: 24 * x)
                          ],
                        ),
                        onPressed: () {
                          if (model.storeInfoComplete) {
                            Navigator.push(context,
                                SlideLeftRoute(widget: AddProductsPage()));
                          }
                        },
                      ),
                    ),
              ),
              SizedBox(height: 25 * y),
            ],
          ),
        ),
      ),
    );
  }
}

class StoreSelectionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 12 * y),
      width: 400 * x,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0 * x),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: Offset(5.0 * x, 1.0 * y),
              blurRadius: 2.0 * x,
              spreadRadius: 3.0 * x)
        ],
      ),
      child: ScopedModelDescendant<NewTransactionModel>(
        builder: (context, child, model) => Column(
              children: <Widget>[
                SizedBox(
                    height: 20 * y, width: MediaQuery.of(context).size.width),
                StoreInput(),
                DateInput(),
                DiscountInput(),
                UserInputFieldSwitches(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text("*" + translate.ifApplicable,
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w700,
                            fontSize: 11 * f)),
                    SizedBox(width: 20 * x),
                  ],
                ),
              ],
            ),
      ),
    );
  }
}

class StoreInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SearchWidget(false),
                    ),
                  );
                  if (result != null) {
                    newTransaction.store = result;
                    newTransaction.isStoreInfoComplete();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.store,
                            color: ColorPallet.darkBlue, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Container(
                          width: 305 * x,
                          child: Text(
                              newTransaction.store != ""
                                  ? newTransaction.store
                                  : translate.enterShop + " ...",
                              style: TextStyle(
                                  color: newTransaction.store != ""
                                      ? ColorPallet.darkBlue
                                      : ColorPallet.midGray,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18 * f),
                              overflow: TextOverflow.ellipsis),
                        )
                      ],
                    ),
                    newTransaction.storeType == ""
                        ? Container()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: 5 * y),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.category,
                                      color: ColorPallet.darkBlue,
                                      size: 27 * x),
                                  SizedBox(width: 10.0 * x),
                                  Container(
                                    width: 305 * x,
                                    child: Text(newTransaction.storeType,
                                        style: TextStyle(
                                            color: ColorPallet.darkBlue
                                                .withOpacity(0.7),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18 * f),
                                        overflow: TextOverflow.ellipsis),
                                  )
                                ],
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translate.shop,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class DateInput extends StatefulWidget {
  @override
  _DateInputState createState() => _DateInputState();
}

class _DateInputState extends State<DateInput> {
  NewTransactionModel newTransaction;
  String hintText = translate.today;
  DateTime initialDate =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  Future<Null> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      locale: Locale(translate.languagePreference),
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(2019),
      lastDate: DateTime(2101),
    );
    picked = DateTime(picked.year, picked.month, picked.day);
    if (picked != null) {
      newTransaction.date = DateFormat('yyyy-MM-dd').format(picked);
      newTransaction.notifyListeners();
    }
  }

  String getDateString(String date) {
    DateTime today =
        DateTime(initialDate.year, initialDate.month, initialDate.day);
    DateTime yesterday =
        DateTime(initialDate.year, initialDate.month, initialDate.day - 1);
    DateTime tomorrow =
        DateTime(initialDate.year, initialDate.month, initialDate.day + 1);

    if (date == DateFormat('yyyy-MM-dd').format(today)) {
      return translate.today;
    } else if (date == DateFormat('yyyy-MM-dd').format(yesterday)) {
      return translate.yesterday;
    } else if (date == DateFormat('yyyy-MM-dd').format(tomorrow)) {
      return translate.tomorrow;
    } else {
      return DateFormat('EEEE, d MMMM', translate.languagePreference)
          .format(DateTime.parse(date));
    }
  }

  @override
  Widget build(BuildContext context) {
    newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10 * y),
              child: InkWell(
                onTap: () async {
                  _selectDate(context);
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.today,
                        color: ColorPallet.darkBlue, size: 27 * x),
                    SizedBox(width: 10.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          getDateString(model.date),
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translate.date,
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class DiscountInput extends StatefulWidget {
  @override
  _DiscountInputState createState() => _DiscountInputState();
}

class _DiscountInputState extends State<DiscountInput> {
  String hintText = translate.enterDiscountReceipt + " ...";

  void changeValue(String value, bool isPercentage) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    setState(() {
      if (double.parse(value) == 0) {
        newTransaction.discountText = translate.noDiscount;
        newTransaction.changeProductDiscountAmount(0);
      } else {
        if (isPercentage) {
          newTransaction.discountText = "-" + value + "%";
          newTransaction.changeProductDiscountPercentage(double.parse(value));
        } else {
          newTransaction.discountAmount = double.parse(value);
          newTransaction.changeProductDiscountAmount(double.parse(value));
          newTransaction.discountText = "-" + value;
        }
      }
    });
  }

  bool isNumeric(String str) {
    if (str == null) {
      return false;
    }
    if (str.substring(str.length - 1) == "%") {
      return true;
    }
    return double.tryParse(str) != null;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return _DiscountDialog(changeValue);
                      });
                },
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.tags,
                        color: ColorPallet.darkBlue, size: 18 * f),
                    SizedBox(width: 17.0 * x),
                    ScopedModelDescendant<NewTransactionModel>(
                      builder: (context, child, model) => Text(
                          model.discountText,
                          style: TextStyle(
                              color: isNumeric(model.discountText)
                                  ? ColorPallet.darkBlue
                                  : ColorPallet.midGray,
                              fontWeight: FontWeight.w500,
                              fontSize: 18 * f)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translate.discountEntireReceipt + "*",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class _DiscountDialog extends StatefulWidget {
  _DiscountDialog(this.changeValue);

  final Function(String value, bool isPercentage) changeValue;

  @override
  _DiscountDialogState createState() => _DiscountDialogState();
}

class _DiscountDialogState extends State<_DiscountDialog> {
  final TextEditingController _percentageController =
      new TextEditingController();
  final TextEditingController _totalController = new TextEditingController();

  String percentageOnChangedInput = "0";
  String totalOnChangedInput = "0";

  void checkPercentage(BuildContext context, String value) {
    value = UserInput.valueSanitizer(value);
    if (UserInput.percentageValidator(value)) {
      value = double.parse(value).toStringAsFixed(0);
      widget.changeValue(value, true);
      Navigator.pop(context);
    } else {
      Toast.show(translate.invalidInput, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  void checkTotal(BuildContext context, String value) {
    value = UserInput.valueSanitizer(value);
    if (UserInput.valueValidator(value)) {
      widget.changeValue(value, false);
      Navigator.pop(context);
    } else {
      Toast.show(translate.invalidInput, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(0),
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: ColorPallet.pink,
              height: 68 * y,
              width: 400 * x,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20 * y),
                  Padding(
                    padding: EdgeInsets.only(left: 35.0 * x),
                    child: Text(translate.enterDiscount,
                        style: TextStyle(
                            fontSize: 25 * f,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40 * y),
            Row(
              children: <Widget>[
                SizedBox(width: 53 * x),
                Column(
                  children: <Widget>[
                    Text(translate.precentage,
                        style: TextStyle(
                            fontSize: 17 * f,
                            fontWeight: FontWeight.w500,
                            color: ColorPallet.darkBlue)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        Container(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text("-",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkBlue))),
                        Container(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: _percentageController,
                            onSubmitted: (val) {
                              checkPercentage(
                                  context, percentageOnChangedInput);
                            },
                            onTap: () {
                              setState(() {
                                _totalController.clear();
                              });
                            },
                            onChanged: (val) {
                              setState(() {
                                percentageOnChangedInput = val;
                              });
                            },
                            keyboardType: TextInputType.numberWithOptions(),
                            decoration: InputDecoration(hintText: "0"),
                          ),
                        ),
                        Container(
                            width: 10 * x,
                            height: 45 * y,
                            child: Text("%",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkBlue))),
                      ],
                    ),
                  ],
                ),
                SizedBox(width: 25 * x),
                Text(translate.or,
                    style: TextStyle(
                        fontSize: 17 * f,
                        fontWeight: FontWeight.w500,
                        color: ColorPallet.darkBlue)),
                SizedBox(width: 35 * x),
                Column(
                  children: <Widget>[
                    Text(translate.total,
                        style: TextStyle(
                            fontSize: 17 * f,
                            fontWeight: FontWeight.w500,
                            color: ColorPallet.darkBlue)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        Container(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text("-",
                                style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkBlue))),
                        Container(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: _totalController,
                            onSubmitted: (val) {
                              checkTotal(context, totalOnChangedInput);
                            },
                            onTap: () {
                              setState(() {
                                _percentageController.clear();
                              });
                            },
                            onChanged: (val) {
                              setState(() {
                                totalOnChangedInput = val;
                              });
                            },
                            keyboardType: TextInputType.numberWithOptions(),
                            decoration: InputDecoration(hintText: "0"),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 30 * y),
            Container(
              width: 400 * x,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      translate.cancel,
                      style:
                          TextStyle(fontSize: 16 * f, color: ColorPallet.pink),
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      if (percentageOnChangedInput != "0") {
                        checkPercentage(context, percentageOnChangedInput);
                      } else {
                        checkTotal(context, totalOnChangedInput);
                      }
                    },
                    child: Text(
                      translate.add,
                      style:
                          TextStyle(fontSize: 16 * f, color: ColorPallet.pink),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10 * y)
          ],
        )
      ],
    );
  }
}

class UserInputFieldSwitches extends StatefulWidget {
  @override
  _UserInputFieldSwitchesState createState() => _UserInputFieldSwitchesState();
}

class _UserInputFieldSwitchesState extends State<UserInputFieldSwitches> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: ScopedModelDescendant<NewTransactionModel>(
              builder: (context, child, model) => Row(
                    children: <Widget>[
                      SizedBox(
                        width: 25.0 * x,
                        height: 26 * y,
                      ),
                      Text(
                        translate.abroad,
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                            fontSize: 18 * f),
                      ),
                      SizedBox(
                        width: 5.0 * x,
                      ),
                      Switch(
                        activeColor: ColorPallet.pink,
                        value: model.abroadExpense,
                        onChanged: (val) {
                          setState(() {
                            model.abroadExpense = !model.abroadExpense;
                          });
                        },
                      ),
                      SizedBox(
                        width: 35.0 * x,
                      ),
                      Text(
                        translate.online,
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                            fontSize: 18 * f),
                      ),
                      SizedBox(
                        width: 5.0 * x,
                      ),
                      Switch(
                        activeColor: ColorPallet.pink,
                        value: model.onlineExpense,
                        onChanged: (val) {
                          setState(() {
                            model.onlineExpense = !model.onlineExpense;
                          });
                        },
                      ),
                    ],
                  ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translate.others + "*",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class StoreCategoryInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);

    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchWidget(false)));
                  if (result != null) {
                    newTransaction.storeType = result;
                    newTransaction.isStoreInfoComplete();
                  }
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.category,
                        color: ColorPallet.darkBlue, size: 27 * f),
                    SizedBox(width: 10.0 * x),
                    Container(
                      width: 305 * x,
                      child: Text(
                        newTransaction.storeType != ""
                            ? newTransaction.storeType
                            : translate.enterShopType + " ...",
                        style: TextStyle(
                            color: newTransaction.storeType != ""
                                ? ColorPallet.darkBlue
                                : ColorPallet.midGray,
                            fontWeight: FontWeight.w500,
                            fontSize: 18 * f),
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translate.shopType,
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0 * f)),
          ),
        ),
      ],
    );
  }
}
