import 'dart:io';

import 'package:budget_onderzoek/pages/new_receipt.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

double x;
double y;
double f;

class CameraPage extends StatefulWidget {
  @override
  _CameraPageState createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return _CameraPageState();
  }
}

class _CameraPageState extends State<CameraPage> {
  CameraController _controller;
  bool _savingImage = false;
  String _imagePath;
  String _dirPath;

  @override
  void initState() {
    super.initState();

    availableCameras().then((cameras) {
      _controller = CameraController(cameras[0], ResolutionPreset.high);
      _controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  void onTakePictureButtonPressed() {
    setState(() {
      _savingImage = true;
    });
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          _imagePath = filePath;
        });
        if (filePath != null) {
          setState(() {
            _savingImage = false;
          });
          print('Picture saved to $filePath');
          NewTransactionModel.of(context).receiptLocation = filePath;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ReceiptDetailsPage(),
            ),
          );
        }
      }
    });
  }

  Future<String> takePicture() async {
    if (!_controller.value.isInitialized) {
      print('Error select camera first');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    _dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(_dirPath).create(recursive: true);
    final String filePath =
        '$_dirPath/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';
    if (_controller.value.isTakingPicture) {
      // A capture is already pending, do nothing
      return null;
    }

    try {
      await _controller.takePicture(filePath);
    } on CameraException catch (e) {
      print(e);
      return null;
    }
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: ColorPallet.lightBlue,
        ));
        Navigator.of(context).pop();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorPallet.pink,
          title: Text(translate.photographReceipt, style: TextStyle(fontSize: 24*f),),
        ),
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(child: _cameraPreviewWidget()),
              ],
            ),
            Positioned(
              bottom: 20,
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 70*x,
                      height: 70*y,
                      child: FittedBox(
                        child: FloatingActionButton(
                          
                          child: Icon(Icons.camera),
                          backgroundColor: ColorPallet.pink,
                          onPressed: () {
                            onTakePictureButtonPressed();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _cameraPreviewWidget() {
    if (_controller == null || !_controller.value.isInitialized) {
      return Container();
    } else {
      return AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: CameraPreview(_controller),
      );
    }
  }
}
