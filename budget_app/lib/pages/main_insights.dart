import 'package:budget_onderzoek/database/datamodels/charts_db.dart';
import 'package:budget_onderzoek/widgets/bar_chart.dart';
import 'package:budget_onderzoek/widgets/donut_chart.dart';
import 'package:budget_onderzoek/widgets/filter_drawer.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';

double x;
double y;
double f;

bool timeChartPage;

class InsightsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    timeChartPage = false;
    return _InsightsPageState();
  }
}

class _InsightsPageState extends State<InsightsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  void showFilterDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  void showTimeChart() {
    setState(() {
      timeChartPage = true;
    });
  }

  void showCategoryChart() {
    setState(() {
      timeChartPage = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      endDrawer: Container(
        width: 290.0 * x,
        child: FliterDrawer(),
      ),
      body: Column(
        children: <Widget>[
          _TopBar(showFilterDrawer, showTimeChart, showCategoryChart),
          SizedBox(height: 10),
          Container(
            width: 175 * x,
            height: 35 * y,
            child: Row(
              children: <Widget>[
                // Icon(Icons.arrow_back_ios,
                //     size: 15, color: ColorPallet.darkBlue),
                SizedBox(width: 20*x),
                Text(translate.mainCategories,
                    style: TextStyle(
                        color: ColorPallet.darkBlue,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0 * f)),
              ],
            ),
          ),
          _CategoryList(),
          SizedBox(height: 15 * y),
          timeChartPage ? _BarChartWidget() : _DonutChartWidget(),
        ],
      ),
    );
  }
}

class _TopBar extends StatelessWidget {
  final void Function() showFilterDrawer;
  final void Function() showTimeChart;
  final void Function() showCategoryChart;

  _TopBar(this.showFilterDrawer, this.showTimeChart, this.showCategoryChart);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: ColorPallet.lightBlue,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        height: 73.9 * y,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(width: 22.0 * x),
                Text(
                  translate.insights,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22.0 * f,
                      fontWeight: FontWeight.w600),
                ),
                Expanded(child: Container()),
                InkWell(
                  child: Icon(Icons.filter_list,
                      size: 33.0 * x, color: Colors.white),
                  onTap: showFilterDrawer,
                ),
                SizedBox(width: 22.0 * x),
              ],
            ),
            Expanded(child: Container()),
            Row(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    showCategoryChart();
                  },
                  child: Container(
                    height: 40 * y,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 6.3),
                        Row(
                          children: <Widget>[
                            Icon(Icons.pie_chart,
                                color: timeChartPage
                                    ? ColorPallet.blueWithOpacity
                                    : Colors.white,
                                size: 20 *x),
                            SizedBox(width: 10 *x),
                            Text(translate.byCategory,
                                style: TextStyle(
                                    color: timeChartPage
                                        ? ColorPallet.blueWithOpacity
                                        : Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14 * f))
                          ],
                        ),
                        Expanded(child: Container()),
                        Container(
                          height: 6 * y,
                          width: MediaQuery.of(context).size.width / 2,
                          color: timeChartPage
                              ? ColorPallet.lightBlue
                              : Colors.white,
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    showTimeChart();
                  },
                  child: Container(
                    height: 40 * y,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 6.3),
                        Row(
                          children: <Widget>[
                            Icon(Icons.equalizer,
                                color: timeChartPage
                                    ? Colors.white
                                    : ColorPallet.blueWithOpacity,
                                size: 20*x),
                            SizedBox(width: 10*x),
                            Text(translate.overTime,
                                style: TextStyle(
                                    fontSize: 14 * f,
                                    color: timeChartPage
                                        ? Colors.white
                                        : ColorPallet.blueWithOpacity,
                                    fontWeight: FontWeight.w600))
                          ],
                        ),
                        Expanded(child: Container()),
                        Container(
                          height: 6 * y,
                          width: MediaQuery.of(context).size.width / 2,
                          color: timeChartPage
                              ? Colors.white
                              : ColorPallet.lightBlue,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }
}

class _BarChartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 278.0 * y,
        margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
        child: BarChart.withSampleData());
  }
}

class _CategoryList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 172 * y,
      child: FutureBuilder(
        future: ChartData.getCategorySpending(),
        builder: (BuildContext context,
            AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (!snapshot.hasData || snapshot.data.length == 0) {
            return Container();
          }
          return ListView.builder(
            scrollDirection: Axis.vertical,
            // padding: EdgeInsets.symmetric(
            //     horizontal: 6.0 * x, vertical: 6 * y),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: <Widget>[
                  _CategoryWidget(snapshot.data[index]),
                ],
              );
            },
          );
        },
      ),
    );
  }
}

class _CategoryWidget extends StatelessWidget {
  _CategoryWidget(this.snapshot);
  final Map<String, dynamic> snapshot;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      width: MediaQuery.of(context).size.width * 0.9,
      height: 35 * y,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Container(
                width: 15 * x,
                height: 15 * y,
                decoration: BoxDecoration(
                  color: snapshot['color'],
                  borderRadius: BorderRadius.circular(3.0 * x),
                )),
            SizedBox(width: 15 * x),
            Container(
              width: 250,
              child: Text(
                  snapshot['name'].toString()[0].toUpperCase() +
                      snapshot['name'].toString().substring(1).toLowerCase(),
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ColorPallet.darkBlue,
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0 * f)),
            ),
            Expanded(
              child: Container(),
            ),
            Text(snapshot['percentage'] + "%",
                style: TextStyle(
                    color: ColorPallet.midGray,
                    fontWeight: FontWeight.w700,
                    fontSize: 16.0 * f))
          ],
        ),
        onPressed: () {},
      ),
    );
  }
}

class _DonutChartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: 278 * y,
          child: FutureBuilder(
              future: ChartData.getDonutChartData(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (!snapshot.hasData) {
                  return Container();
                }
                return DonutChart(snapshot.data, animate: true);
              }),
        ),
        Center(
          child: Container(
            height: 278 * y,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Totaal:",
                    style: TextStyle(
                        color: ColorPallet.darkBlue,
                        fontWeight: FontWeight.w800,
                        fontSize: 17.0 * f)),
                SizedBox(height: 10.0 * y),
                FutureBuilder(
                  future: ChartData.getTotalSpend(),
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    if (!snapshot.hasData) {
                      return Text("€0",
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w800,
                              fontSize: 19.0 * f));
                    } else {
                      return Text("€" + snapshot.data,
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w800,
                              fontSize: 19.0 * f));
                    }
                  },
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
