import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/quick_search.dart';
import 'package:budget_onderzoek/util/string_matching_products.dart';
import 'package:budget_onderzoek/util/string_matching_shops.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';

double x;
double y;
double f;

class SearchWidget extends StatefulWidget {
  SearchWidget(this.isProductSearch);

  final bool isProductSearch;

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
    return _SearchWidgetState();
  }
}

class _SearchWidgetState extends State<SearchWidget> {
  final TextEditingController _controller = new TextEditingController();
  FocusNode myFocusNode;
  String searchString = "";
  List<Map<String, dynamic>> results;

  Future<List<Map<String, dynamic>>> getSearchResults() async {
    if (widget.isProductSearch) {
      results = await StringMatchingProducts.getSearchResults(searchString);
    } else {
      results = await StringMatchingShops.getSearchResults(searchString);
    }

    return results;
  }

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  bool inputChecker() {
    searchString = UserInput.textSanitizer(searchString);
    if (!UserInput.textValidator(searchString) || searchString.length == 0) {
      Toast.show(translate.invalidInput, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      setState(() {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      });
      return false;
    } else {
      return true;
    }
  }

  String formatSearchString(String searchStringTest) {
    if (searchStringTest == null) {
      return "";
    }
    if (searchStringTest.length > 0) {
      return searchString[0].toUpperCase() + searchString.substring(1);
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return WillPopScope(
      onWillPop: () {
        SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: ColorPallet.pink),
        );
        Navigator.of(context).pop();
      },
      child: Scaffold(
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                  height: 73.0 * y,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    border:
                        Border.all(width: 13.0 * x, color: ColorPallet.pink),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: TextField(
                            autocorrect: false,
                            focusNode: myFocusNode,
                            keyboardType: TextInputType.text,
                            onSubmitted: (value) {},
                            onChanged: (value) {
                              setState(() {
                                searchString = value;
                              });
                            },
                            autofocus: true,
                            controller: _controller,
                            style: TextStyle(
                                color: ColorPallet.darkBlue,
                                fontSize: 20.0 * f,
                                fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              hintText: widget.isProductSearch
                                  ? translate.findProductService + " ..."
                                  : translate.findShop + " ...",
                              hintStyle: TextStyle(
                                  color: ColorPallet.midGray,
                                  fontSize: 18.0 * f),
                              prefixIcon: InkWell(
                                child: Icon(
                                  Icons.arrow_back,
                                  color: ColorPallet.midGray,
                                  size: 24 * x,
                                ),
                                onTap: () {
                                  SystemChrome.setSystemUIOverlayStyle(
                                    SystemUiOverlayStyle(
                                      statusBarColor: ColorPallet.pink,
                                    ),
                                  );
                                  Navigator.of(context).pop();
                                },
                              ),

                              // filled: true,
                              // fillColor: Colors.white,
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ),
                      searchString.length == 0
                          ? Container()
                          : InkWell(
                              child: Container(
                                width: 110 * x,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10 * x, vertical: 10 * y),
                                decoration: BoxDecoration(
                                    color: ColorPallet.pink,
                                    borderRadius: BorderRadius.circular(10 * x),
                                    boxShadow: [
                                      BoxShadow(
                                        color: ColorPallet.lightGray,
                                        offset: Offset(1.0 * x, 1.0 * y),
                                        blurRadius: 1.5 * x,
                                        spreadRadius: 1.5 * x,
                                      )
                                    ]),
                                child: Center(
                                  child: Text(translate.notFound + "?",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13 * f,
                                          fontWeight: FontWeight.w500)),
                                ),
                              ),
                              onTap: () {
                                if (inputChecker()) {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return _SearchNotFoundDialog(
                                            searchString,
                                            results,
                                            widget.isProductSearch);
                                      });
                                }
                              },
                            ),
                    ],
                  ),
                ),
                SizedBox(height: 4 * y),
                Expanded(
                  child: FutureBuilder(
                    future: getSearchResults(),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                      if (!snapshot.hasData ||
                          snapshot.data.length == 0 ||
                          searchString.length == 0) {
                        return _QuickSuggestionList(widget.isProductSearch);
                      }
                      return ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _SearchResultTile(
                              snapshot.data[index]['examples'][0]
                                      .toString()[0]
                                      .toUpperCase() +
                                  snapshot.data[index]['examples'][0]
                                      .toString()
                                      .substring(1),
                              snapshot.data[index]['category'].toString(),
                              widget.isProductSearch);
                        },
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _QuickSuggestionList extends StatelessWidget {
  _QuickSuggestionList(this.isProductSearch);
  final bool isProductSearch;

  Future<List<String>> getSearchResults(BuildContext context) async {
    if (isProductSearch) {
      return await QuickSearch.getProductSuggestions(
          ScopedModel.of<NewTransactionModel>(context).storeType);
    } else {
      return await QuickSearch.getShopSuggestions(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getSearchResults(context),
      builder: (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
        if (!snapshot.hasData || snapshot.data.length == 0) {
          return Container();
        }
        return Column(
          children: <Widget>[
            SizedBox(height: 2 * y),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          if (isProductSearch) {
                            ScopedModel.of<NewTransactionModel>(context)
                                .product = snapshot.data[index];
                            ScopedModel.of<NewTransactionModel>(context)
                                .productCategory = snapshot.data[index];
                          } else {
                            ScopedModel.of<NewTransactionModel>(context).store =
                                snapshot.data[index];
                            ScopedModel.of<NewTransactionModel>(context)
                                .storeType = snapshot.data[index];
                          }
                          NewTransactionModel.of(context)
                              .isProductInfoComplete();

                          NewTransactionModel.of(context)
                              .isReceiptInfoComplete();
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 17 * x, vertical: 10 * y),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 5,
                                child: Text(
                                  snapshot.data[index],
                                  style: TextStyle(
                                    fontSize: 16 * f,
                                    fontWeight: FontWeight.w700,
                                    color: ColorPallet.darkBlue,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Icon(Icons.arrow_forward_ios,
                                    color: ColorPallet.midGray, size: 18 * f),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 17 * x, vertical: 2 * y),
                        width: MediaQuery.of(context).size.width,
                        height: 1,
                        color: ColorPallet.lightGray,
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}

class _SearchResultTile extends StatelessWidget {
  const _SearchResultTile(this.headerText, this.subText, this.isProductSearch);

  final String headerText;
  final String subText;
  final bool isProductSearch;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InkWell(
          // behavior: HitTestBehavior.translucent,
          onTap: () {
            if (isProductSearch) {
              ScopedModel.of<NewTransactionModel>(context).product = headerText;
              ScopedModel.of<NewTransactionModel>(context).productCategory =
                  subText;
            } else {
              ScopedModel.of<NewTransactionModel>(context).store = headerText;
              ScopedModel.of<NewTransactionModel>(context).storeType = subText;
            }
            NewTransactionModel.of(context).isProductInfoComplete();
            NewTransactionModel.of(context).isReceiptInfoComplete();
            Navigator.of(context).pop();
          },
          // onVerticalDragStart: (_) {
          //   FocusScope.of(context).requestFocus(new FocusNode());
          // },
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 17 * x, vertical: 2 * y),
                      child: Text(
                        headerText,
                        style: TextStyle(
                            fontSize: 16 * f,
                            fontWeight: FontWeight.w700,
                            color: ColorPallet.darkBlue),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 17 * x),
                      child: Text(
                        subText,
                        style: TextStyle(
                            fontSize: 12.5 * f,
                            color: ColorPallet.darkBlue.withOpacity(0.8),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                  ],
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Icon(Icons.arrow_forward_ios,
                      color: ColorPallet.midGray, size: 18 * f))
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 17 * x, vertical: 2 * y),
          width: MediaQuery.of(context).size.width,
          height: 1 * y,
          color: ColorPallet.lightGray,
        )
      ],
    );
  }
}

class _SearchNotFoundDialog extends StatefulWidget {
  _SearchNotFoundDialog(this.searchString, this.results, this.isProductSearch);

  final String searchString;
  final List<Map<String, dynamic>> results;
  final bool isProductSearch;

  @override
  _SearchNotFoundDialogState createState() => _SearchNotFoundDialogState();
}

class _SearchNotFoundDialogState extends State<_SearchNotFoundDialog> {
  int selectedRadioTile;
  int selectedRadio;

  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(0),
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: ColorPallet.pink,
              height: 68 * y,
              width: 400 * x,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20 * y),
                  Padding(
                    padding: EdgeInsets.only(left: 35.0 * x),
                    child: Text(translate.addYourself,
                        style: TextStyle(
                            fontSize: 23 * f,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 15 * y,
                    width: 400 * x,
                  ),
                  Text(
                    widget.isProductSearch ? translate.productService + ":" : translate.shop,
                    style: TextStyle(
                      fontSize: 14 * f,
                      fontWeight: FontWeight.w500,
                      color: ColorPallet.darkBlue.withOpacity(0.8),
                    ),
                  ),
                  SizedBox(height: 5 * y),
                  Text(
                    widget.searchString[0].toUpperCase() +
                        widget.searchString.substring(1),
                    style: TextStyle(
                      fontSize: 20 * f,
                      fontWeight: FontWeight.w600,
                      color: ColorPallet.darkBlue,
                    ),
                  ),
                  SizedBox(
                    height: 15 * y,
                    width: 400 * x,
                  ),
                  Text(
                    translate.belongsTo + ":",
                    style: TextStyle(
                      fontSize: 14 * f,
                      fontWeight: FontWeight.w500,
                      color: ColorPallet.darkBlue.withOpacity(0.8),
                    ),
                  ),
                  SizedBox(height: 10 * y),
                  Column(
                    children: <Widget>[
                      RadioListTile(
                        dense: true,
                        value: 0,
                        groupValue: selectedRadioTile,
                        title: Text(
                          widget.results[0]['category'],
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onChanged: (val) {
                          print("Radio Tile pressed $val");
                          setSelectedRadioTile(val);
                        },
                        activeColor: ColorPallet.pink,
                      ),
                      RadioListTile(
                        dense: true,
                        value: 1,
                        groupValue: selectedRadioTile,
                        title: Text(
                          widget.results[1]['category'],
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onChanged: (val) {
                          print("Radio Tile pressed $val");
                          setSelectedRadioTile(val);
                        },
                        activeColor: ColorPallet.pink,
                      ),
                      RadioListTile(
                        dense: true,
                        value: 2,
                        groupValue: selectedRadioTile,
                        title: Text(
                          widget.results[2]['category'],
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onChanged: (val) {
                          print("Radio Tile pressed $val");
                          setSelectedRadioTile(val);
                        },
                        activeColor: ColorPallet.pink,
                      ),
                      RadioListTile(
                        dense: true,
                        value: 3,
                        groupValue: selectedRadioTile,
                        title: Text(
                          widget.results[3]['category'],
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onChanged: (val) {
                          print("Radio Tile pressed $val");
                          setSelectedRadioTile(val);
                        },
                        activeColor: ColorPallet.pink,
                      ),
                      RadioListTile(
                        dense: true,
                        value: 4,
                        groupValue: selectedRadioTile,
                        title: Text(
                          translate.others,
                          style: TextStyle(
                            fontSize: 14 * f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        onChanged: (val) {
                          print("Radio Tile pressed $val");
                          setSelectedRadioTile(val);
                        },
                        activeColor: ColorPallet.pink,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            child: Text(translate.cancel,
                                style: TextStyle(
                                  color: ColorPallet.pink,
                                  fontSize: 14 * f,
                                )),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text(translate.add,
                                style: TextStyle(
                                  color: ColorPallet.pink,
                                  fontSize: 14 * f,
                                )),
                            onPressed: () {
                              String example =
                                  widget.searchString[0].toUpperCase() +
                                      widget.searchString.substring(1);

                              String category = [
                                widget.results[0]['category'],
                                widget.results[1]['category'],
                                widget.results[2]['category'],
                                widget.results[3]['category'],
                                translate.others
                              ][selectedRadioTile];
                              if (widget.isProductSearch) {
                                ScopedModel.of<NewTransactionModel>(context)
                                    .product = example;
                                ScopedModel.of<NewTransactionModel>(context)
                                    .productCategory = category;
                              } else {
                                ScopedModel.of<NewTransactionModel>(context)
                                    .store = example;
                                ScopedModel.of<NewTransactionModel>(context)
                                    .storeType = category;
                              }
                              NewTransactionModel.of(context)
                                  .isProductInfoComplete();
                              NewTransactionModel.of(context)
                                  .isReceiptInfoComplete();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
