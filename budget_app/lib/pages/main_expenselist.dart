import 'dart:async';

import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/pages/new_camera.dart';
import 'package:budget_onderzoek/pages/new_receipt.dart';
import 'package:budget_onderzoek/pages/new_store.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/category_icon_map.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:budget_onderzoek/widgets/filter_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';

double x;
double y;
double f;

String today;
String yesterday;
String lastDate;

class TransactionsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TransactionsPageState();
  }
}

class _TransactionsPageState extends State<TransactionsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool searchEnabled = false;
  bool variableExpense = true;

  void enableSearch() {
    setState(() {
      searchEnabled = true;
    });
  }

  void disableSearch() {
    setState(() {
      searchEnabled = false;
      ExpenseListModel.of(context).reset();
    });
  }

  void enableVariableExpense() {
    setState(() {
      variableExpense = true;
    });
  }

  void disableVariableExpense() {
    setState(() {
      variableExpense = false;
    });
  }

  void openFilterDrawer() {
    _scaffoldKey.currentState.openEndDrawer();
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      key: _scaffoldKey,
      endDrawer: Container(
        width: 295.0 * x,
        child: FliterDrawer(),
      ),
      body: Column(
        children: <Widget>[
          _TopBarWidget(
              searchEnabled, enableSearch, disableSearch, openFilterDrawer),
          Expanded(child: _ExpenseListWidget())
        ],
      ),
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  final bool searchEnabled;
  final Function() enableSearch;
  final Function() disableSearch;
  final Function() openFilterDrawer;

  _TopBarWidget(this.searchEnabled, this.enableSearch, this.disableSearch,
      this.openFilterDrawer);

  @override
  Widget build(BuildContext context) {
    List<Widget> searchEnabledWidgets = [
      SizedBox(width: 22.0 * x),
      Text(
        translate.transactions,
        style: TextStyle(
            color: Colors.white,
            fontSize: 22.0 * f,
            fontWeight: FontWeight.w600),
      ),
      Expanded(child: Container()),
      InkWell(
        child: Icon(Icons.search, size: 33.0 * x, color: Colors.white),
        onTap: enableSearch,
      ),
      SizedBox(width: 5.0 * x),
      InkWell(
        child: Icon(Icons.filter_list, size: 33.0 * x, color: Colors.white),
        onTap: openFilterDrawer,
      ),
      SizedBox(width: 22.0 * x),
    ];

    List<Widget> searchDisabled = [
      SizedBox(width: 12.0 * x),
      Container(
        width: 350.0 * x,
        child: TextField(
          onChanged: (value) {
            ExpenseListModel.of(context).searchWord = value;
            ExpenseListModel.of(context).notifyListeners();
          },
          autofocus: true,
          style: TextStyle(color: Colors.white, fontSize: 20.0 * f),
          decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.search,
                color: ColorPallet.veryLightBlue,
                size: 24 * x,
              ),
              hintText: translate.findTransactions + " ...",
              hintStyle:
                  TextStyle(color: ColorPallet.veryLightBlue, fontSize: 18.0)),
        ),
      ),
      InkWell(
        child: Icon(
          Icons.close,
          color: Colors.white,
          size: 33.0 * x,
        ),
        onTap: disableSearch,
      ),
    ];

    return Container(
      height: 50.0 * x,
      color: ColorPallet.lightBlue,
      child:
          Row(children: searchEnabled ? searchDisabled : searchEnabledWidgets),
    );
  }
}

class _ExpenseListWidget extends StatelessWidget {
  Future<List<Map<String, dynamic>>> getTransactions(context, filtermodel) async {
    List<Map<String, dynamic>> allRows =
        await TransactionDatabase.queryAllTransactionRows(context, filtermodel);

    allRows = allRows.reversed.toList();

    String lastDate = "";
    annotedTransactions = [];

    allRows.forEach((transaction) {
      String newDate = transaction['date'];
      if (newDate != lastDate) {
        annotedTransactions.add(transaction['transactionID']);
        lastDate = newDate;
      }
    });

    return allRows;
  }

  List<String> annotedTransactions;
  String today = DateFormat('yyyy-MM-dd').format(DateTime.now());
  String yesterday = DateFormat('yyyy-MM-dd')
      .format(DateTime.now().subtract(new Duration(days: 1)));
  String lastTransactionID;
  Widget getDateWidget(Map<String, dynamic> transaction) {
    String newDate = transaction['date'];
    Widget dateHeader;

    if (annotedTransactions != null) {
      if (annotedTransactions.contains(transaction['transactionID'])) {
        if (newDate == today) {
          newDate = translate.today;
        } else if (newDate == yesterday) {
          newDate = translate.yesterday;
        } else {
          newDate = DateFormat('EEEE, d MMMM', translate.languagePreference)
              .format(DateTime.parse(newDate));
          newDate = newDate[0].toUpperCase() + newDate.substring(1);
        }
        dateHeader = Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0*x, vertical: 10*y),
          child: Text(newDate,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15.0*f,
                  fontWeight: FontWeight.w500)),
        );
      } else {
        dateHeader = Container();
      }
    } else {
      dateHeader = Container();
    }
    return dateHeader;
  }

  @override
  Widget build(BuildContext context) {
    lastDate = "";
    lastTransactionID = "";
    return ScopedModelDescendant<FilterModel>(
      builder: (_context2, _child2, filtermodel) => ScopedModelDescendant<ExpenseListModel>(
        builder: (context, child, expensemodel) =>
            ScopedModelDescendant<NewTransactionModel>(
              builder: (_context1, _child1, transactionmodel) => FutureBuilder(
                    future: getTransactions(context, filtermodel),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                      if (!snapshot.hasData || snapshot.data.length == 0) {
                        return _NoExpenseWidget();
                      }
                      return ListView.builder(
                        scrollDirection: Axis.vertical,
                        padding: EdgeInsets.symmetric(
                            horizontal: 6.0 * x, vertical: 6 * y),
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: <Widget>[
                              getDateWidget(snapshot.data[index]),
                              _StoreItemWidget(snapshot.data[index]),
                            ],
                          );
                        },
                      );
                    },
                  ),
            ),
      ),
    );
  }
}

class _NoExpenseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          "images/no_items_found_icon.png",
          height: 85.0 * y,
          width: 85.0 * x,
        ),
        SizedBox(height: 10.0 * y),
        Text(
          translate.noExpensesFound + " ...",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: ColorPallet.midGray,
              fontWeight: FontWeight.w700,
              fontSize: 14.0 * f),
        ),
      ],
    );
  }
}

class _StoreItemWidget extends StatelessWidget {
  _StoreItemWidget(this.transaction);

  final Map<String, dynamic> transaction;

  Future<List<Map<String, dynamic>>> getProducts(String transaction) async {
    List<Map<String, dynamic>> allRows =
        await TransactionDatabase.queryTransactionProducts(transaction);
    allRows = allRows.reversed.toList();
    return allRows;
  }

  Widget discountWidget() {
    if (double.parse(transaction['discountAmount']) > 0 ||
        double.parse(transaction['discountPercentage']) > 0) {
      String discount;

      if (double.parse(transaction['discountAmount']) > 0) {
        discount = "-" + transaction['discountAmount'];
      }

      if (double.parse(transaction['discountPercentage']) > 0) {
        discount = "-" + transaction['discountPercentage'] + "%";
      }

      return Container(
        padding: EdgeInsets.symmetric(vertical: 7),
        child: Row(
          children: <Widget>[
            SizedBox(width: 30*x),
            Text(translate.discount,
                style: TextStyle(
                    fontSize: 14*f,
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w700)),
            Expanded(
              child: Container(),
            ),
            Text(discount,
                style: TextStyle(
                    fontSize: 14*f,
                    color: ColorPallet.pink,
                    fontWeight: FontWeight.w700)),
            SizedBox(width: 58*x),
          ],
        ),
      );
    }

    return Container();
  }

  Icon getStoreIcon(String storeType) {
    if (IconMap.icon.containsKey(storeType)) {
      return Icon(IconMap.icon[storeType][0],
          color: ColorPallet.darkBlue, //IconMap.icon[storeType][1],
          size: IconMap.icon[storeType][2] ? 19*x : 25*x);
    }
    return Icon(Icons.store, size: 25*x, color: ColorPallet.darkBlue);
  }

  @override
  Widget build(BuildContext context) {
    String store = transaction['store'];
    String storeType = transaction['storeType'];
    String totalPrice = transaction['totalPrice'];
    String receiptLocation = transaction['receiptLocation'];

    if (double.parse(totalPrice) > 1000) {
      totalPrice = double.parse(totalPrice).toStringAsFixed(0);
    }

    return Container(
        margin: EdgeInsets.symmetric(horizontal: 6.0 * x, vertical: 6.0 * y),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0*x, 1.0*y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: Theme(
          data: ThemeData(accentColor: ColorPallet.lightBlue),
          child: ExpansionTile(
            
            title: Row(
              children: <Widget>[
                Container(
                  width: 145*x,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        store,
                        style: TextStyle(
                            fontSize: 18*f,
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w700),
                      ),
                      Text(
                        storeType,
                        style: TextStyle(
                            fontSize: 14*f,
                            color: ColorPallet.midGray,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                receiptLocation == ""
                    ? Container()
                    : Icon(
                        Icons.camera_alt,
                        color: ColorPallet.darkBlue,
                        size: 24*x,
                      ),
                receiptLocation == "" ? Container() : SizedBox(width: 10*x),
                Expanded(child: Container()),
                Text(
                  totalPrice,
                  style: TextStyle(
                      fontSize: 16.5*f,
                      color: ColorPallet.pink,
                      fontWeight: FontWeight.w700),
                ),
              ],
            ),
            leading: getStoreIcon(storeType),
            children: <Widget>[
              SizedBox(height: 5*y),
              SizedBox(height: 5*y),
              receiptLocation == ""
                  ? Container()
                  : Image.asset(receiptLocation),
              receiptLocation != ""
                  ? Container()
                  : FutureBuilder(
                      future: getProducts(transaction['transactionID']),
                      builder: (BuildContext context,
                          AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
                        if (!snapshot.hasData) {
                          return Container();
                        }
                        return Column(
                            children: snapshot.data.map((productInfo) {
                          return _ProductItemWidget(productInfo);
                        }).toList());
                      },
                    ),
              SizedBox(height: 5*y),
              receiptLocation != "" ? Container() : discountWidget(),
              Container(
                height: 65*y,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 130*x,
                      height: 30*y,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0*x),
                        ),
                        color: ColorPallet.darkBlue,
                        child: Text(translate.delete,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16.0*f)),
                        onPressed: () {
                          TransactionDatabase.deleteTransaction(
                              transaction['transactionID']);
                          NewTransactionModel.of(context).notifyListeners();
                        },
                      ),
                    ),
                    SizedBox(width: 20*x),
                    Container(
                      width: 125*x,
                      height: 28*y,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0*x),
                        ),
                        color: ColorPallet.darkBlue,
                        child: Text(
                          translate.edit,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16.0*f),
                        ),
                        onPressed: () async {
                          await NewTransactionModel.of(context)
                              .modifyTransaction(transaction['transactionID']);

                          if (receiptLocation == "") {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SelectStorePage(),
                              ),
                            );
                          } else {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ReceiptDetailsPage(),
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class _ProductItemWidget extends StatelessWidget {
  _ProductItemWidget(this.productInfo);

  final Map<String, dynamic> productInfo;

  String product;
  String productCategory;
  String price;
  String discount;

  @override
  Widget build(BuildContext context) {
    product = productInfo['product'];
    productCategory = productInfo['productCategory'];
    price = productInfo['price'];

    return Padding(
      padding: EdgeInsets.only(top: 8.0*y, bottom: 8.0*y, left: 30*x),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 250*x,
                child: Text(
                  product,
                  style: TextStyle(
                      fontSize: 14*f,
                      color: ColorPallet.darkBlue,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(height: 5*y),
              Container(
                width: 250*x,
                child: Text(
                  productCategory,
                  style: TextStyle(
                      fontSize: 14*f,
                      color: ColorPallet.midGray,
                      fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            children: <Widget>[
              Text(
                price,
                style: TextStyle(
                    fontSize: 14*f,
                    color: ColorPallet.pink,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 5*y),
              Text(" "),
            ],
          ),
          SizedBox(width: 58*x),
        ],
      ),
    );
  }
}
