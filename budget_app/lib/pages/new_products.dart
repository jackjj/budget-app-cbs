import 'dart:async';

import 'package:budget_onderzoek/pages/search.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/string_matching_products.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path/path.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

double x;
double y;
double f;

class AddProductsPage extends StatelessWidget {

  Future<void> loadProductsDatabase() async{
    StringMatchingProducts.loadProductTable();
  }

  @override
  Widget build(BuildContext context) {
    loadProductsDatabase();
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;
    textController.text = "";
    return ScopedModelDescendant<NewTransactionModel>(
      builder: (context, child, model) => Scaffold(
            appBar: AppBar(
              backgroundColor: ColorPallet.pink,
              title: Row(
                children: <Widget>[
                  Container(
                    width: 240 * x,
                    child: Text(
                      model.store,
                      style: TextStyle(color: Colors.white, fontSize: 23 * f),
                      overflow: TextOverflow.fade,
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Text(
                    model.totalPrice.toStringAsFixed(2),
                    style: TextStyle(color: Colors.white, fontSize: 23 * f),
                  ),
                  SizedBox(
                    width: 10 * x,
                  )
                ],
              ),
            ),
            body: Column(
              children: <Widget>[
                SizedBox(
                  height: 7 * y,
                  width: MediaQuery.of(context).size.width,
                ),
                ProductDescriptionList(),
                SizedBox(height: 12 * y),
                Text(translate.purchasedfrom + " " + model.store + ':',
                    style: TextStyle(
                        color: ColorPallet.darkBlue,
                        fontSize: 16 * f,
                        fontWeight: FontWeight.w600)),
                SizedBox(height: 12 * y),
                Expanded(child: ProductListWidget()),
                SizedBox(height: 10 * y),
                Container(
                  width: 120 * x,
                  height: 35 * y,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0 * x),
                    ),
                    color: model.productCounter > 0
                        ? ColorPallet.darkBlue
                        : ColorPallet.midGray,
                    child: Text(translate.complete,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0 * f)),
                    onPressed: () async {
                      if (model.productInfoComplete) {
                        await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(
                                    translate.warning), //TODO: resize text?
                                content: Text(translate.youHaveNotAdded +
                                    ' "' +
                                    model.product +
                                    '" ' +
                                    translate.addBeforeCompletion),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(translate.no,
                                        style:
                                            TextStyle(color: ColorPallet.pink)),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(translate.yes,
                                        style:
                                            TextStyle(color: ColorPallet.pink)),
                                    onPressed: () {
                                      model.addNewProduct();
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              );
                            });
                      }

                      model.addNewTransAction();
                      SystemChrome.setSystemUIOverlayStyle(
                        SystemUiOverlayStyle(
                            statusBarColor: ColorPallet.lightBlue),
                      );
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                SizedBox(height: 25 * y)
              ],
            ),
          ),
    );
  }
}

class ProductDescriptionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<NewTransactionModel>(
      builder: (context, child, model) => Container(
            width: 400 * x,
            padding: EdgeInsets.only(bottom: 12 * y),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(5.0 * x),
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: ColorPallet.veryLightGray,
                    offset: Offset(5.0 * x, 1.0 * y),
                    blurRadius: 2.0 * x,
                    spreadRadius: 3.0 * x)
              ],
            ),
            child: Column(
              children: <Widget>[
                SizedBox(
                    height: 20 * y, width: MediaQuery.of(context).size.width),
                ProductInput(),
                PriceInput(),
                SizedBox(height: 5 * y),
                Row(
                  children: <Widget>[
                    SizedBox(width: 65 * x),
                    InkWell(
                      onTap: () {
                        model.subtractItemMultiplier();
                      },
                      child: Icon(
                        Icons.indeterminate_check_box,
                        size: 30 * x,
                        color: model.productInfoComplete
                            ? ColorPallet.darkBlue
                            : ColorPallet.midGray,
                      ),
                    ),
                    SizedBox(width: 5 * x),
                    Container(
                      width: 35 * x,
                      child: Center(
                        child: Text(
                          model.itemMultiplier + "x",
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 20 * f,
                            color: model.productInfoComplete
                                ? ColorPallet.darkBlue
                                : ColorPallet.midGray,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 5 * x),
                    InkWell(
                      onTap: () {
                        model.addItemMultiplier();
                      },
                      child: Icon(
                        Icons.add_box,
                        size: 30 * x,
                        color: model.productInfoComplete
                            ? ColorPallet.darkBlue
                            : ColorPallet.midGray,
                      ),
                    ),
                    SizedBox(width: 23 * x),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0 * x),
                      ),
                      color: model.productInfoComplete
                          ? ColorPallet.darkBlue
                          : ColorPallet.midGray,
                      child: Text(
                        translate.add,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0 * f),
                      ),
                      onPressed: () {
                        if (model.productInfoComplete) {
                          NewTransactionModel.of(context).price = double.parse(
                                  NewTransactionModel.of(context).price)
                              .toStringAsFixed(2);
                          textController.text = "";
                          model.addNewProduct();
                          FocusScope.of(context).requestFocus(new FocusNode());
                        }
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
    );
  }
}

class ProductInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchWidget(true)));
                  if (result != null) {
                    newTransaction.product = result;
                    newTransaction.isProductInfoComplete();
                  }
                },
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.label,
                            color: ColorPallet.darkBlue, size: 27 * x),
                        SizedBox(width: 10.0 * x),
                        Container(
                          width: 305 * x,
                          child: Text(
                            newTransaction.product != ""
                                ? newTransaction.product
                                : translate.enterProductService + " ...",
                            style: TextStyle(
                                color: newTransaction.product != ""
                                    ? ColorPallet.darkBlue
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w500,
                                fontSize: 18 * f),
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                    newTransaction.productCategory == ""
                        ? Container()
                        : Column(
                            children: <Widget>[
                              SizedBox(height: 5 * y),
                              Row(
                                children: <Widget>[
                                  Icon(Icons.category,
                                      color: ColorPallet.darkBlue,
                                      size: 27 * x),
                                  SizedBox(width: 10.0 * x),
                                  Container(
                                    width: 305 * x,
                                    child: Text(newTransaction.productCategory,
                                        style: TextStyle(
                                            color: ColorPallet.darkBlue
                                                .withOpacity(0.7),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18 * f),
                                        overflow: TextOverflow.ellipsis),
                                  )
                                ],
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translate.productService,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class ProductCategoryInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () async {
                  String result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchWidget(true)));
                  if (result != null) {
                    newTransaction.productCategory = result;
                    newTransaction.isProductInfoComplete();
                  }
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.category,
                        color: ColorPallet.darkBlue, size: 27 * x),
                    SizedBox(width: 10.0 * x),
                    Container(
                      width: 305 * x,
                      child: Text(
                        newTransaction.productCategory != ""
                            ? newTransaction.productCategory
                            : translate.enterCatagory + " ...",
                        style: TextStyle(
                            color: newTransaction.productCategory != ""
                                ? ColorPallet.darkBlue
                                : ColorPallet.midGray,
                            fontWeight: FontWeight.w500,
                            fontSize: 18 * f),
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translate.category,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class TextEditingControllerWorkaroud extends TextEditingController {
  TextEditingControllerWorkaroud({String text}) : super(text: text);

  void setTextAndPosition(String newText, {int caretPosition}) {
    int offset = caretPosition != null ? caretPosition : newText.length;
    value = value.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: offset),
        composing: TextRange.empty);
  }
}

class PriceInput extends StatefulWidget {
  @override
  _PriceInputState createState() => _PriceInputState();
}

TextEditingControllerWorkaroud textController =
    TextEditingControllerWorkaroud();

class _PriceInputState extends State<PriceInput> {
  String price = "";
  void checkUserInput(String newPrice, BuildContext context) {
    //newPrice = UserInput.valueSanitizer(newPrice);
    if (!UserInput.valueValidator(newPrice)) {
      Toast.show(translate.invalidInput, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      textController.setTextAndPosition(price);
    } else {
      price = newPrice;
      NewTransactionModel.of(context).price = price;
      NewTransactionModel.of(context).isProductInfoComplete();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(0.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                border:
                    Border.all(color: ColorPallet.lightGray, width: 1.7 * x),
                borderRadius: BorderRadius.circular(12.0 * x)),
            child: Row(
              children: <Widget>[
                SizedBox(width: 10.0),
                Icon(Icons.local_offer,
                    color: ColorPallet.darkBlue, size: 27 * x),
                // SizedBox(width: 3.0),
                Container(
                  alignment: Alignment.topLeft,
                  width: 200 * x,
                  height: 40 * y,
                  margin: EdgeInsets.only(bottom: 3.5 * x, top: 6 * y),
                  child: TextField(
                    controller: textController,
                    style: TextStyle(
                        color: ColorPallet.darkBlue,
                        fontWeight: FontWeight.w500,
                        fontSize: 18 * f),
                    autocorrect: false,
                    keyboardType: TextInputType.number,
                    onChanged: (newPrice) {
                      setState(() {
                        checkUserInput(newPrice, context);
                      });
                    },
                    onSubmitted: (value) {
                      if (NewTransactionModel.of(context).price != "") {
                        price = double.parse(price).toStringAsFixed(2);
                        textController.text = price;
                        NewTransactionModel.of(context).price = price;
                        NewTransactionModel.of(context).isProductInfoComplete();
                      }
                    },
                    decoration: InputDecoration(
                      hintText: NewTransactionModel.of(context).price != ""
                          ? NewTransactionModel.of(context).price
                          : translate.enterPrice + " ...",
                      hintStyle: TextStyle(
                          color: ColorPallet.midGray,
                          fontWeight: FontWeight.w500,
                          fontSize: 18 * f),
                      filled: true,
                      fillColor: Colors.transparent,
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 48,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(
              translate.price,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontWeight: FontWeight.w700,
                  fontSize: 14 * f),
            ),
          ),
        ),
      ],
    );
  }
}

class ProductListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScopedModelDescendant<NewTransactionModel>(
        builder: (context, child, model) => ListView.builder(
              itemCount: model.products.length,
              itemBuilder: (BuildContext context, int index) {
                return ProductTile(model.products[index]);
              },
            ),
      ),
    );
  }
}

class ProductTile extends StatelessWidget {
  ProductTile(this.productInfo);
  final Map<String, String> productInfo;

  @override
  Widget build(BuildContext context) {
    NewTransactionModel newTransaction = NewTransactionModel.of(context);
    String price = productInfo['price'];
    String product = productInfo['product'];
    String productCategory = productInfo['productCategory'];
    return Container(
      height: 50 * y,
      margin: EdgeInsets.symmetric(horizontal: 7.0 * x, vertical: 4 * y),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0 * x),
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: Offset(5.0 * x, 1.0 * y),
              blurRadius: 2.0 * x,
              spreadRadius: 3.0 * x)
        ],
      ),
      child: Row(
        children: <Widget>[
          SizedBox(width: 13 * x),
          Container(
            width: 70 * x,
            child: Text(
              price,
              style: TextStyle(
                  fontSize: 18 * f,
                  fontWeight: FontWeight.w600,
                  color: ColorPallet.darkBlue),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(width: 15 * x),
          Container(
            width: 220 * x,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(product,
                    style: TextStyle(
                        fontSize: 16.5 * f,
                        fontWeight: FontWeight.w600,
                        color: ColorPallet.darkBlue),
                    overflow: TextOverflow.ellipsis),
                Text(productCategory,
                    style: TextStyle(
                        fontSize: 14 * f,
                        fontWeight: FontWeight.w600,
                        color: ColorPallet.midGray),
                    overflow: TextOverflow.ellipsis),
              ],
            ),
          ),
          SizedBox(width: 15 * x),
          InkWell(
            onTap: () {
              newTransaction.deleteProduct(productInfo);
              newTransaction.editProduct(productInfo);
            },
            child: Icon(FontAwesomeIcons.solidEdit,
                color: ColorPallet.darkBlue, size: 16 * f),
          ),
          SizedBox(width: 15 * x),
          InkWell(
              onTap: () {
                newTransaction.deleteProduct(productInfo);
              },
              child:
                  Icon(Icons.close, color: ColorPallet.darkBlue, size: 25 * x)),
        ],
      ),
    );
  }
}
