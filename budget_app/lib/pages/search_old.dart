import 'package:budget_onderzoek/util/user_input.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';

class SearchWidget extends StatefulWidget {
  SearchWidget(this.topBarColor, this.textInput);

  final Color topBarColor;
  final bool textInput;

  @override
  State<StatefulWidget> createState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor:  ColorPallet.pink,
    ));
    return _SearchWidgetState();
  }
}

class _SearchWidgetState extends State<SearchWidget> {
  final TextEditingController _controller = new TextEditingController();
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  void _returnSearchValue(String str) {
    if (widget.textInput) {
      str = UserInput.textSanitizer(str);
      if (!UserInput.textValidator(str)) {
        Toast.show(translate.invalidInput, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        setState(() {
          _controller.clear();
          FocusScope.of(context).requestFocus(myFocusNode);
        });
        return;
      }
    } else {
      str = UserInput.valueSanitizer(str);
      if (!UserInput.valueValidator(str)) {
       Toast.show(translate.invalidAmount, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
        setState(() {
          FocusScope.of(context).requestFocus(myFocusNode);
        });
        return;
      }
    }
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: ColorPallet.pink),
    );
    Navigator.pop(context, str);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: ColorPallet.pink),
        );
        Navigator.of(context).pop();
      },
      child: Scaffold(
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                  height: 73.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    border: Border.all(width: 13.0, color: widget.topBarColor),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: TextField(
                            autocorrect: false,
                            focusNode:  myFocusNode,
                            keyboardType: widget.textInput ? TextInputType.text : TextInputType.number,
                            onSubmitted: _returnSearchValue,
                            autofocus: true,
                            controller: _controller,
                            style: TextStyle(color: ColorPallet.darkBlue, fontSize: 20.0, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              prefixIcon: InkWell(
                                child: Icon(Icons.arrow_back, color: ColorPallet.midGray),
                                onTap: () {
                                  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
                                    statusBarColor: ColorPallet.pink,
                                  ));
                                  Navigator.of(context).pop();
                                },
                              ),
                              suffixIcon: InkWell(
                                child: Icon(Icons.close, color: ColorPallet.midGray),
                                onTap: () {
                                  _controller.clear();
                                },
                              ),
                              hintStyle: TextStyle(),
                              filled: true,
                              fillColor: Colors.white,
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
