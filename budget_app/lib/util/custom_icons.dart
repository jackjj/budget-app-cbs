import 'package:flutter/widgets.dart';

class CustomIcons {
  static const IconData papers = const IconData(0xe800, fontFamily: "CustomIcons");
  static const IconData box = const IconData(0xf187, fontFamily: "CustomIcons");
  static const IconData utilities = const IconData(0xe802, fontFamily: "CustomIcons");
  static const IconData variable_or_fixed = const IconData(0xe803, fontFamily: "CustomIcons");
  static const IconData scan_receipt = const IconData(0xe804, fontFamily: "CustomIcons");
}