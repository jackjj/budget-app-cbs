import 'package:flutter/material.dart';

class ColorPallet {
  static const Color darkBlue = Color(0xFF33425B);
  static const Color midblue = Color(0xFF271D6C);
  static const Color lightBlue = Color(0xFF00A1CD);
  static const Color veryLightBlue = Color(0xFFE5F6FA);
  static const Color hiddenBlue = Color(0xFF33B4D7);
  static const Color blueWithOpacity = Color(0xFF80D0E6);

  static const Color pink = Color(0xFFAF0E80);  
  
  static const Color darkGreen = Color(0xFF488225);
  static const Color lightGreen = Color(0xFFAFCB05);

  static const Color orange = Color(0xFFF39200);


  
  static const Color lightGray = Color(0xFFDDDDDD);
  static const Color midGray = Color(0xFFAAAAAA);
  static const Color veryLightGray = Color(0xFFEFEFEF);
}
