class TextStrings {
static const String infomation = """Door het gebruik van deze app helpt u mee aan het budget onderzoek van het centraal bureau statistiek.

Als dank voor uw help mogen wij u aan het einde van het onderzoek een VVV bon ter waarde van 10 euro aanbieden. Om in aanraking te komen voor deze bon gelden de volgende regels:

1) Gedurende 30 dagen houdt u iedere dag al uw uitgaven bij in deze app. Zowel uw vaste lasten, als ook variabele uitgaven.
2) Aan het einde van iedere dag verifieert u dat u alle uitgaven heeft bijgewerkt. Ook als u die dag niets heeft uitgegeven.
3) U doet mee aan dit onderzoek binnen de onderzoeks periode: van .... tot .....""";
}