import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/custom_icons.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconMap {
  static Map<String, List<dynamic>> icon = {
    "Benzinestation": [Icons.local_gas_station, ColorPallet.orange, false],
    "Warenhuis (V&D, Hema, e.d.)": [
      FontAwesomeIcons.building,
      ColorPallet.orange, true
    ],
    "Warenhuis": [FontAwesomeIcons.building, ColorPallet.orange, true],
    "Winkel met uitgebreid assortiment (Blokker, Xenos, e.d.)": [
      FontAwesomeIcons.shoppingBag,
      ColorPallet.orange, true
    ],
    "Supermarkt": [FontAwesomeIcons.shoppingCart, ColorPallet.orange, true],
    "Bakker / banketbakker": [FontAwesomeIcons.breadSlice, ColorPallet.orange, true],
    "Groenten- en fruitwinkel": [FontAwesomeIcons.carrot, ColorPallet.orange, true],
    "Slagerij / poelier": [FontAwesomeIcons.drumstickBite, ColorPallet.orange, true],
    "Viswinkel": [FontAwesomeIcons.fish, ColorPallet.orange, true],
    "Zuivelwinkel": [FontAwesomeIcons.cheese, ColorPallet.orange, true],
    "Slijterij, drankhandel": [FontAwesomeIcons.wineBottle, ColorPallet.orange, true],
    "Slijterij": [FontAwesomeIcons.wineBottle, ColorPallet.orange, true],
    "Overige winkels voor levensmiddelen (gespecialiseerd)": [
      Icons.store,
      ColorPallet.orange, false
    ],
    "Tabakswinkel": [FontAwesomeIcons.smoking, ColorPallet.orange, true],
    "Kledingwinkel, textielwinkels": [
      FontAwesomeIcons.tshirt,
      ColorPallet.orange, true
    ],
    "Kledingwinkel of textielwinkels": [
      FontAwesomeIcons.tshirt,
      ColorPallet.orange, true
    ],
    "Schoenenwinkel": [FontAwesomeIcons.shoePrints, ColorPallet.orange, true],
    "Woninginrichting (meubelen, vloerbedekking, gordijnen, e.d.)": [
      FontAwesomeIcons.couch,
      ColorPallet.orange, true
    ],
    "Winkel voor verlichting": [FontAwesomeIcons.lightbulb, ColorPallet.orange, true],
    "Elektronicawinkel, witgoedwinkel": [
      Icons.tv,
      ColorPallet.orange, false
    ],"Elektronicawinkel of witgoedwinkel": [
      Icons.tv,
      ColorPallet.orange, false
    ],
    "Computerwinkel": [Icons.computer, ColorPallet.orange, false],
    "Huishoudelijke artikelen": [FontAwesomeIcons.broom, ColorPallet.orange, true],
    "Bouwmarkt": [FontAwesomeIcons.hammer, ColorPallet.orange, true],
    "Bloemenwinkel, tuincentrum": [
      FontAwesomeIcons.seedling,
      ColorPallet.orange, true
    ],
    "Fiets- of bromfietswinkel": [FontAwesomeIcons.bicycle, ColorPallet.orange, true],
    "Speelgoedwinkel": [Icons.gamepad, ColorPallet.orange, false],
    "Foto- en filmzaak": [Icons.camera_alt, ColorPallet.orange, false],
    "Optiekwinkel/brillenzaak, juwelier": [
      FontAwesomeIcons.glasses,
      ColorPallet.orange, true
    ],
    "Kantoor-/boekhandel, kiosk": [FontAwesomeIcons.book, ColorPallet.orange],
    "Sportartikelen en buitenrecreatie": [
      FontAwesomeIcons.footballBall,
      ColorPallet.orange, true
    ],
    "Apotheek": [FontAwesomeIcons.prescriptionBottleAlt, ColorPallet.orange, true],
    "Drogisterij, parfumerie": [Icons.spa, ColorPallet.orange, false],
    "Drogisterij of parfumerie": [Icons.spa, ColorPallet.orange, false],
    "Souvenir-, cadeauwinkel": [FontAwesomeIcons.gifts, ColorPallet.orange, true],
    "Overige winkels (niet voor levensmiddelen)": [
      Icons.store,
      ColorPallet.orange, false
    ],
    "Telefoonwinkel": [FontAwesomeIcons.mobileAlt, ColorPallet.orange, true],
    "Platenzaak/cd-winkel": [FontAwesomeIcons.compactDisc, ColorPallet.orange, true],
    "Garage": [FontAwesomeIcons.warehouse, ColorPallet.orange, true],
    "2e hands gekocht bij bedrijf of winkel": [Icons.sync, ColorPallet.orange, false],
    "2e hands gekocht van particulier": [Icons.sync, ColorPallet.orange, false],
    "Winkel voor lederwaren/reisartikelen": [
      Icons.beach_access,
      ColorPallet.orange, false
    ],
    "Antiekwinkel": [FontAwesomeIcons.chessKnight, ColorPallet.orange, true],
    "Doe-het-zelfwinkel": [FontAwesomeIcons.tools, ColorPallet.orange, true],
    "Winkel voor ijzerwaren/verf/hout e.d.": [
      FontAwesomeIcons.fillDrip,
      ColorPallet.orange, true
    ],
    "Dierenwinkel": [Icons.pets, ColorPallet.orange, false],
    "Markt, braderie, op straat gekocht": [
      Icons.shopping_basket,
      ColorPallet.orange, false
    ],
    "Postorderbedrijf": [FontAwesomeIcons.cube, ColorPallet.orange, true],
    "Aan de deur, rijdende winkel": [
      FontAwesomeIcons.truck,
      ColorPallet.orange, true
    ],
    "Groothandel": [FontAwesomeIcons.cubes, ColorPallet.orange, true],
    "Boer of tuinder": [FontAwesomeIcons.tractor, ColorPallet.orange, true],
    "Bedrijfskantine": [Icons.business, ColorPallet.orange, false],
    "Sportkantine": [Icons.local_dining, ColorPallet.orange, false],
    "Horeca (hotel, café, restaurant, ijssalon, snackbar, e.d.)": [
      Icons.restaurant,
      ColorPallet.orange, false
    ],
    "Horeca": [
      Icons.restaurant,
      ColorPallet.orange, false
    ],
    "Alle overige aankopen en diensten": [Icons.category, ColorPallet.orange, false],
    "Verkocht": [Icons.person, ColorPallet.orange, false],
    "Gift": [FontAwesomeIcons.gift, ColorPallet.orange, true],
  };
}
