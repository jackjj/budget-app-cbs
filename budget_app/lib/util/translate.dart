library translate;

final Translate translate = Translate._private();

class Translate {
  String languagePreference;

  Translate._private();

  

  String get infoBody => _infoBody[languagePreference];
  Map<String, String> _infoBody = <String, String>{
    "nl":
        """Door het gebruik van deze app helpt u mee aan het budget onderzoek van het centraal bureau statistiek.

Als dank voor uw help mogen wij u aan het einde van het onderzoek een VVV bon ter waarde van 10 euro aanbieden. Om in aanraking te komen voor deze bon gelden de volgende regels:

1) Gedurende 30 dagen houdt u iedere dag al uw uitgaven bij in deze app. Zowel uw vaste lasten, als ook variabele uitgaven.
2) Aan het einde van iedere dag verifieert u dat u alle uitgaven heeft bijgewerkt. Ook als u die dag niets heeft uitgegeven.
3) U doet mee aan dit onderzoek binnen de onderzoeks periode: van .... tot .....""",
    "en":
        """Through the use of this app you are helping us conduct research towards household budgets.

As a sign of our appreciation we can offer you a gift certificate worth 10 dollars, if you complete this survey. To be eligable for this gift certifcate you will have to admit to the following rules:

1. For a period of 30 days you keep track of all your expenses. Both your fixed/recurring expenses, such as rent and utilities, as well as variable expenses.
2. At the end of each day you verify that you have kept track of all the expenses for that day. Even if you did not purchaser anything on that particular day.
3. You participate within the research period. Which is between … and …."""
  };

  //Generated code below this line. Do not change manually!!!!!!!!!!!!!!!!!!!!


  String get key => _key[languagePreference];
  Map<String, String> _key = <String, String>{
    "nl": "nl", 
    "en": "en", 
    "sl": "sl", 
    "fi": "fi"
  };

  String get fixedExpensesFilled => _fixedExpensesFilled[languagePreference];
  Map<String, String> _fixedExpensesFilled = <String, String>{
    "nl": "Vaste uitgaven ingevuld", 
    "en": "Fixed expenses filled out", 
    "sl": "", 
    "fi": ""
  };

  String get variablexExpensesFilled => _variablexExpensesFilled[languagePreference];
  Map<String, String> _variablexExpensesFilled = <String, String>{
    "nl": "Variabele uitgaven", 
    "en": "Variabele expenses", 
    "sl": "", 
    "fi": ""
  };

  String get info => _info[languagePreference];
  Map<String, String> _info = <String, String>{
    "nl": "Info", 
    "en": "Info", 
    "sl": "", 
    "fi": ""
  };

  String get infoTitle => _infoTitle[languagePreference];
  Map<String, String> _infoTitle = <String, String>{
    "nl": "Informatie budget onderzoek", 
    "en": "Information budget research", 
    "sl": "", 
    "fi": ""
  };

  String get infoForMoreInfo => _infoForMoreInfo[languagePreference];
  Map<String, String> _infoForMoreInfo = <String, String>{
    "nl": "Voor meer informatie kunt u terecht", 
    "en": "For more information please visit", 
    "sl": "", 
    "fi": ""
  };

  String get infoWebsiteLink => _infoWebsiteLink[languagePreference];
  Map<String, String> _infoWebsiteLink = <String, String>{
    "nl": "op de website van het CBS.", 
    "en": "our website.", 
    "sl": "", 
    "fi": ""
  };

  String get variableExpenses => _variableExpenses[languagePreference];
  Map<String, String> _variableExpenses = <String, String>{
    "nl": "Variabele uitgaven", 
    "en": "Variable expenses", 
    "sl": "", 
    "fi": ""
  };

  String get fixedExpenses => _fixedExpenses[languagePreference];
  Map<String, String> _fixedExpenses = <String, String>{
    "nl": "Vaste lasten", 
    "en": "Fixed expenses", 
    "sl": "", 
    "fi": ""
  };

  String get selectFilter => _selectFilter[languagePreference];
  Map<String, String> _selectFilter = <String, String>{
    "nl": "Filter selecteren", 
    "en": "Select filter", 
    "sl": "", 
    "fi": ""
  };

  String get people => _people[languagePreference];
  Map<String, String> _people = <String, String>{
    "nl": "Personen", 
    "en": "People", 
    "sl": "", 
    "fi": ""
  };

  String get fixedOrVariable => _fixedOrVariable[languagePreference];
  Map<String, String> _fixedOrVariable = <String, String>{
    "nl": "Vast en variabel", 
    "en": "Fixed or variable", 
    "sl": "", 
    "fi": ""
  };

  String get removeFilters => _removeFilters[languagePreference];
  Map<String, String> _removeFilters = <String, String>{
    "nl": "Filters wissen", 
    "en": "Remove filters", 
    "sl": "", 
    "fi": ""
  };

  String get ifApplicable => _ifApplicable[languagePreference];
  Map<String, String> _ifApplicable = <String, String>{
    "nl": "Indien van toepassing", 
    "en": "If applicable", 
    "sl": "", 
    "fi": ""
  };

  String get abroad => _abroad[languagePreference];
  Map<String, String> _abroad = <String, String>{
    "nl": "Buitenland", 
    "en": "Abroad", 
    "sl": "", 
    "fi": ""
  };

  String get online => _online[languagePreference];
  Map<String, String> _online = <String, String>{
    "nl": "Online", 
    "en": "Online", 
    "sl": "", 
    "fi": ""
  };

  String get others => _others[languagePreference];
  Map<String, String> _others = <String, String>{
    "nl": "Overige", 
    "en": "Additional", 
    "sl": "", 
    "fi": ""
  };

  String get purchasedfrom => _purchasedfrom[languagePreference];
  Map<String, String> _purchasedfrom = <String, String>{
    "nl": "Gekocht bij", 
    "en": "Purchased from", 
    "sl": "", 
    "fi": ""
  };

  String get youHaveNotAdded => _youHaveNotAdded[languagePreference];
  Map<String, String> _youHaveNotAdded = <String, String>{
    "nl": "U heeft nog niet toegevoegd", 
    "en": "You have not yet added", 
    "sl": "", 
    "fi": ""
  };

  String get addBeforeCompletion => _addBeforeCompletion[languagePreference];
  Map<String, String> _addBeforeCompletion = <String, String>{
    "nl": "Toevoegen voordat u de bestelling afrond?", 
    "en": "Add before you complete the order?", 
    "sl": "", 
    "fi": ""
  };

  String get photographReceipt => _photographReceipt[languagePreference];
  Map<String, String> _photographReceipt = <String, String>{
    "nl": "Bon fotograferen", 
    "en": "Photograph receipt", 
    "sl": "", 
    "fi": ""
  };

  String get transactions => _transactions[languagePreference];
  Map<String, String> _transactions = <String, String>{
    "nl": "Transacties", 
    "en": "Transactions", 
    "sl": "", 
    "fi": ""
  };

  String get findTransactions => _findTransactions[languagePreference];
  Map<String, String> _findTransactions = <String, String>{
    "nl": "Zoek transacties", 
    "en": "Find transactions", 
    "sl": "", 
    "fi": ""
  };

  String get earnedReward => _earnedReward[languagePreference];
  Map<String, String> _earnedReward = <String, String>{
    "nl": "Beloning verdiend", 
    "en": "Earned reward", 
    "sl": "", 
    "fi": ""
  };

  String get moreInfo => _moreInfo[languagePreference];
  Map<String, String> _moreInfo = <String, String>{
    "nl": "Meer informatie", 
    "en": "More information", 
    "sl": "", 
    "fi": ""
  };

  String get insights => _insights[languagePreference];
  Map<String, String> _insights = <String, String>{
    "nl": "Inzichten", 
    "en": "Insights", 
    "sl": "", 
    "fi": ""
  };

  String get belongsTo => _belongsTo[languagePreference];
  Map<String, String> _belongsTo = <String, String>{
    "nl": "Behoort tot", 
    "en": "Belongs to", 
    "sl": "", 
    "fi": ""
  };

  String get addYourself => _addYourself[languagePreference];
  Map<String, String> _addYourself = <String, String>{
    "nl": "Zelf toevoegen", 
    "en": "Add yourself", 
    "sl": "", 
    "fi": ""
  };

  String get addInfo => _addInfo[languagePreference];
  Map<String, String> _addInfo = <String, String>{
    "nl": "Informatie toevoegen", 
    "en": "Add information", 
    "sl": "", 
    "fi": ""
  };

  String get title => _title[languagePreference];
  Map<String, String> _title = <String, String>{
    "nl": "titel", 
    "en": "title", 
    "sl": "", 
    "fi": ""
  };

  String get contentDay => _contentDay[languagePreference];
  Map<String, String> _contentDay = <String, String>{
    "nl": "inhoud dag", 
    "en": "content day", 
    "sl": "", 
    "fi": ""
  };

  String get shop => _shop[languagePreference];
  Map<String, String> _shop = <String, String>{
    "nl": "Winkel", 
    "en": "Shop", 
    "sl": "", 
    "fi": ""
  };

  String get enterShop => _enterShop[languagePreference];
  Map<String, String> _enterShop = <String, String>{
    "nl": "Voer winkel in", 
    "en": "Enter shop", 
    "sl": "", 
    "fi": ""
  };

  String get findShop => _findShop[languagePreference];
  Map<String, String> _findShop = <String, String>{
    "nl": "Zoek winkel", 
    "en": "Find shop", 
    "sl": "", 
    "fi": ""
  };

  String get selectShop => _selectShop[languagePreference];
  Map<String, String> _selectShop = <String, String>{
    "nl": "Winkel selecteren", 
    "en": "Select Shop", 
    "sl": "", 
    "fi": ""
  };

  String get shopType => _shopType[languagePreference];
  Map<String, String> _shopType = <String, String>{
    "nl": "Winkeltype", 
    "en": "Shop type", 
    "sl": "", 
    "fi": ""
  };

  String get enterShopType => _enterShopType[languagePreference];
  Map<String, String> _enterShopType = <String, String>{
    "nl": "Voer winkeltype in", 
    "en": "Enter shop type", 
    "sl": "", 
    "fi": ""
  };

  String get products => _products[languagePreference];
  Map<String, String> _products = <String, String>{
    "nl": "Producten", 
    "en": "Products", 
    "sl": "", 
    "fi": ""
  };

  String get productService => _productService[languagePreference];
  Map<String, String> _productService = <String, String>{
    "nl": "Product / Dienst", 
    "en": "Product / Service", 
    "sl": "", 
    "fi": ""
  };

  String get enterProductService => _enterProductService[languagePreference];
  Map<String, String> _enterProductService = <String, String>{
    "nl": "Voer product/dienst in", 
    "en": "Enter Product / Service", 
    "sl": "", 
    "fi": ""
  };

  String get findProductService => _findProductService[languagePreference];
  Map<String, String> _findProductService = <String, String>{
    "nl": "Zoek product/dienst", 
    "en": "Find product/service", 
    "sl": "", 
    "fi": ""
  };

  String get category => _category[languagePreference];
  Map<String, String> _category = <String, String>{
    "nl": "Categorie", 
    "en": "Category", 
    "sl": "", 
    "fi": ""
  };

  String get categories => _categories[languagePreference];
  Map<String, String> _categories = <String, String>{
    "nl": "Categorieën", 
    "en": "Categories", 
    "sl": "", 
    "fi": ""
  };

  String get enterCatagory => _enterCatagory[languagePreference];
  Map<String, String> _enterCatagory = <String, String>{
    "nl": "Voer categorie in", 
    "en": "Enter catagory", 
    "sl": "", 
    "fi": ""
  };

  String get mainCategories => _mainCategories[languagePreference];
  Map<String, String> _mainCategories = <String, String>{
    "nl": "Hoofdcategorieën", 
    "en": "Main categories", 
    "sl": "", 
    "fi": ""
  };

  String get byCategory => _byCategory[languagePreference];
  Map<String, String> _byCategory = <String, String>{
    "nl": "Per categorie", 
    "en": "By category", 
    "sl": "", 
    "fi": ""
  };

  String get time => _time[languagePreference];
  Map<String, String> _time = <String, String>{
    "nl": "Tijdstip", 
    "en": "Time", 
    "sl": "", 
    "fi": ""
  };

  String get monday => _monday[languagePreference];
  Map<String, String> _monday = <String, String>{
    "nl": "M", 
    "en": "M", 
    "sl": "", 
    "fi": ""
  };

  String get tuesday => _tuesday[languagePreference];
  Map<String, String> _tuesday = <String, String>{
    "nl": "D", 
    "en": "T", 
    "sl": "", 
    "fi": ""
  };

  String get wednesday => _wednesday[languagePreference];
  Map<String, String> _wednesday = <String, String>{
    "nl": "W", 
    "en": "W", 
    "sl": "", 
    "fi": ""
  };

  String get thursday => _thursday[languagePreference];
  Map<String, String> _thursday = <String, String>{
    "nl": "D", 
    "en": "T", 
    "sl": "", 
    "fi": ""
  };

  String get friday => _friday[languagePreference];
  Map<String, String> _friday = <String, String>{
    "nl": "V", 
    "en": "F", 
    "sl": "", 
    "fi": ""
  };

  String get saturday => _saturday[languagePreference];
  Map<String, String> _saturday = <String, String>{
    "nl": "Z", 
    "en": "S", 
    "sl": "", 
    "fi": ""
  };

  String get sunday => _sunday[languagePreference];
  Map<String, String> _sunday = <String, String>{
    "nl": "Z", 
    "en": "S", 
    "sl": "", 
    "fi": ""
  };

  String get today => _today[languagePreference];
  Map<String, String> _today = <String, String>{
    "nl": "Vandaag", 
    "en": "Today", 
    "sl": "", 
    "fi": ""
  };

  String get yesterday => _yesterday[languagePreference];
  Map<String, String> _yesterday = <String, String>{
   "nl": "Gisteren", 
    "en": "Yesterday", 
    "sl": "", 
    "fi": ""
  };

  String get tomorrow => _tomorrow[languagePreference];
  Map<String, String> _tomorrow = <String, String>{
    "nl": "Morgen", 
    "en": "Tomorrow", 
    "sl": "", 
    "fi": ""
  };

  String get daysCompleted => _daysCompleted[languagePreference];
  Map<String, String> _daysCompleted = <String, String>{
    "nl": "dagen ingevuld", 
    "en": "days completed", 
    "sl": "", 
    "fi": ""
  };

  String get daysMissing => _daysMissing[languagePreference];
  Map<String, String> _daysMissing = <String, String>{
    "nl": "dagen ontbreken", 
    "en": "days missing", 
    "sl": "", 
    "fi": ""
  };

  String get daysRemaining => _daysRemaining[languagePreference];
  Map<String, String> _daysRemaining = <String, String>{
    "nl": "dagen resterend", 
    "en": "days remaining", 
    "sl": "", 
    "fi": ""
  };

  String get date => _date[languagePreference];
  Map<String, String> _date = <String, String>{
    "nl": "Datum", 
    "en": "Date", 
    "sl": "", 
    "fi": ""
  };

  String get beginDate => _beginDate[languagePreference];
  Map<String, String> _beginDate = <String, String>{
    "nl": "Start datum", 
    "en": "Begin date", 
    "sl": "", 
    "fi": ""
  };

  String get endDate => _endDate[languagePreference];
  Map<String, String> _endDate = <String, String>{
    "nl": "Eind datum", 
    "en": "End date", 
    "sl": "", 
    "fi": ""
  };

  String get dailyReminders => _dailyReminders[languagePreference];
  Map<String, String> _dailyReminders = <String, String>{
    "nl": "Dagelijkse herinnering", 
    "en": "Daily reminder", 
    "sl": "", 
    "fi": ""
  };

  String get overTime => _overTime[languagePreference];
  Map<String, String> _overTime = <String, String>{
   "nl": "Over tijd", 
    "en": "Over time", 
    "sl": "", 
    "fi": ""
  };

  String get discount => _discount[languagePreference];
  Map<String, String> _discount = <String, String>{
    "nl": "Korting", 
    "en": "Discount", 
    "sl": "", 
    "fi": ""
  };

  String get enterTotalPrice => _enterTotalPrice[languagePreference];
  Map<String, String> _enterTotalPrice = <String, String>{
    "nl": "Voer totaal prijs in", 
    "en": "Enter total price", 
    "sl": "", 
    "fi": ""
  };

  String get totalPrice => _totalPrice[languagePreference];
  Map<String, String> _totalPrice = <String, String>{
    "nl": "Totaal prijs", 
    "en": "Total price", 
    "sl": "", 
    "fi": ""
  };

  String get enterPrice => _enterPrice[languagePreference];
  Map<String, String> _enterPrice = <String, String>{
    "nl": "Voer prijs in", 
    "en": "Enter price", 
    "sl": "", 
    "fi": ""
  };

  String get price => _price[languagePreference];
  Map<String, String> _price = <String, String>{
    "nl": "Prijs", 
    "en": "Price", 
    "sl": "", 
    "fi": ""
  };

  String get priceRange => _priceRange[languagePreference];
  Map<String, String> _priceRange = <String, String>{
    "nl": "Bedragen", 
    "en": "Price range", 
    "sl": "", 
    "fi": ""
  };

  String get enterDiscountReceipt => _enterDiscountReceipt[languagePreference];
  Map<String, String> _enterDiscountReceipt = <String, String>{
    "nl": "Voer korting op bon in", 
    "en": "Enter discount on receipt", 
    "sl": "", 
    "fi": ""
  };

  String get noDiscount => _noDiscount[languagePreference];
  Map<String, String> _noDiscount = <String, String>{
    "nl": "Geen korting", 
    "en": "No discount", 
    "sl": "", 
    "fi": ""
  };

  String get discountEntireReceipt => _discountEntireReceipt[languagePreference];
  Map<String, String> _discountEntireReceipt = <String, String>{
    "nl": "Korting op gehele bon", 
    "en": "Discount on entire receipt", 
    "sl": "", 
    "fi": ""
  };

  String get enterDiscount => _enterDiscount[languagePreference];
  Map<String, String> _enterDiscount = <String, String>{
    "nl": "Korting invoeren", 
    "en": "Enter discount", 
    "sl": "", 
    "fi": ""
  };

  String get precentage => _precentage[languagePreference];
  Map<String, String> _precentage = <String, String>{
    "nl": "Percentage", 
    "en": "Percentage", 
    "sl": "", 
    "fi": ""
  };

  String get invalidAmount => _invalidAmount[languagePreference];
  Map<String, String> _invalidAmount = <String, String>{
    "nl": "Ongeldig bedrag", 
    "en": "Invalid amount", 
    "sl": "", 
    "fi": ""
  };

  String get total => _total[languagePreference];
  Map<String, String> _total = <String, String>{
    "nl": "Totaal", 
    "en": "Total", 
    "sl": "", 
    "fi": ""
  };

  String get noExpensesFound => _noExpensesFound[languagePreference];
  Map<String, String> _noExpensesFound = <String, String>{
    "nl": "Geen uitgaven gevonden", 
    "en": "No expenses found", 
    "sl": "", 
    "fi": ""
  };

  String get discountExpensesDiscount => _discountExpensesDiscount[languagePreference];
  Map<String, String> _discountExpensesDiscount = <String, String>{
    "nl": "Korting", 
    "en": "Discount", 
    "sl": "", 
    "fi": ""
  };

  String get filterAmount => _filterAmount[languagePreference];
  Map<String, String> _filterAmount = <String, String>{
    "nl": "Bedrag filteren", 
    "en": "Filter amount", 
    "sl": "", 
    "fi": ""
  };

  String get no => _no[languagePreference];
  Map<String, String> _no = <String, String>{
    "nl": "Nee", 
    "en": "No", 
    "sl": "", 
    "fi": ""
  };

  String get yes => _yes[languagePreference];
  Map<String, String> _yes = <String, String>{
    "nl": "Ja", 
    "en": "Yes", 
    "sl": "", 
    "fi": ""
  };

  String get delete => _delete[languagePreference];
  Map<String, String> _delete = <String, String>{
    "nl": "Verwijderen", 
    "en": "Delete", 
    "sl": "", 
    "fi": ""
  };

  String get edit => _edit[languagePreference];
  Map<String, String> _edit = <String, String>{
    "nl": "Aanpassen", 
    "en": "Edit", 
    "sl": "", 
    "fi": ""
  };

  String get complete => _complete[languagePreference];
  Map<String, String> _complete = <String, String>{
    "nl": "Afronden", 
    "en": "Complete", 
    "sl": "", 
    "fi": ""
  };

  String get warning => _warning[languagePreference];
  Map<String, String> _warning = <String, String>{
    "nl": "Waarschuwing", 
    "en": "Warning", 
    "sl": "", 
    "fi": ""
  };

  String get or => _or[languagePreference];
  Map<String, String> _or = <String, String>{
    "nl": "of", 
    "en": "or", 
    "sl": "", 
    "fi": ""
  };

  String get add => _add[languagePreference];
  Map<String, String> _add = <String, String>{
    "nl": "Toevoegen", 
    "en": "Add", 
    "sl": "", 
    "fi": ""
  };

  String get cancel => _cancel[languagePreference];
  Map<String, String> _cancel = <String, String>{
    "nl": "Annuleren", 
    "en": "Cancel", 
    "sl": "", 
    "fi": ""
  };

  String get onValue => _onValue[languagePreference];
  Map<String, String> _onValue = <String, String>{
    "nl": "Aan", 
    "en": "On", 
    "sl": "", 
    "fi": ""
  };

  String get minimum => _minimum[languagePreference];
  Map<String, String> _minimum = <String, String>{
    "nl": "Minimum", 
    "en": "Minimum", 
    "sl": "", 
    "fi": ""
  };

  String get maximum => _maximum[languagePreference];
  Map<String, String> _maximum = <String, String>{
    "nl": "Maximum", 
    "en": "Maximum", 
    "sl": "", 
    "fi": ""
  };

  String get none => _none[languagePreference];
  Map<String, String> _none = <String, String>{
    "nl": "Geen", 
    "en": "None", 
    "sl": "", 
    "fi": ""
  };

  String get everything => _everything[languagePreference];
  Map<String, String> _everything = <String, String>{
    "nl": "Alles", 
    "en": "Everything", 
    "sl": "", 
    "fi": ""
  };

  String get invalidInput => _invalidInput[languagePreference];
  Map<String, String> _invalidInput = <String, String>{
    "nl": "Ongeldige invoer", 
    "en": "Invalid input", 
    "sl": "", 
    "fi": ""
  };

  String get notFound => _notFound[languagePreference];
  Map<String, String> _notFound = <String, String>{
    "nl": "Niet gevonden", 
    "en": "Not found", 
    "sl": "", 
    "fi": ""
  };

  String get close => _close[languagePreference];
  Map<String, String> _close = <String, String>{
    "nl": "Sluiten", 
    "en": "Close", 
    "sl": "", 
    "fi": ""
  };

  String get ok => _ok[languagePreference];
  Map<String, String> _ok = <String, String>{
    "nl": "ok", 
    "en": "ok", 
    "sl": "", 
    "fi": ""
  };

  String get settings => _settings[languagePreference];
  Map<String, String> _settings = <String, String>{
    "nl": "Instellingen", 
    "en": "Settings", 
    "sl": "", 
    "fi": ""
  };

  String get preferences => _preferences[languagePreference];
  Map<String, String> _preferences = <String, String>{
    "nl": "Voorkeuren", 
    "en": "Preferences", 
    "sl": "", 
    "fi": ""
  };

  String get language => _language[languagePreference];
  Map<String, String> _language = <String, String>{
    "nl": "Taal", 
    "en": "Language", 
    "sl": "", 
    "fi": ""
  };

  String get currency => _currency[languagePreference];
  Map<String, String> _currency = <String, String>{
    "nl": "Munteenheid", 
    "en": "Currency", 
    "sl": "", 
    "fi": ""
  };

  String get family => _family[languagePreference];
  Map<String, String> _family = <String, String>{
    "nl": "Familie", 
    "en": "Family", 
    "sl": "", 
    "fi": ""
  };

  String get familyCode => _familyCode[languagePreference];
 Map<String, String> _familyCode = <String, String>{
    "nl": "Familie code", 
    "en": "Family code", 
    "sl": "", 
    "fi": ""
  };

  String get shareWithFamily => _shareWithFamily[languagePreference];
  Map<String, String> _shareWithFamily = <String, String>{
    "nl": "Uitgaven delen", 
    "en": "Share expenses", 
    "sl": "", 
    "fi": ""
  };

  String get familyMembers => _familyMembers[languagePreference];
  Map<String, String> _familyMembers = <String, String>{
    "nl": "Familie leden", 
    "en": "Family code", 
    "sl": "", 
    "fi": ""
  };

  String get notifications => _notifications[languagePreference];
  Map<String, String> _notifications = <String, String>{
    "nl": "Notificaties", 
    "en": "Notifications", 
    "sl": "", 
    "fi": ""
  };

  String get contact => _contact[languagePreference];
  Map<String, String> _contact = <String, String>{
    "nl": "Contact", 
    "en": "Contact", 
    "sl": "", 
    "fi": ""
  };

  String get phoneNumber => _phoneNumber[languagePreference];
  Map<String, String> _phoneNumber = <String, String>{
    "nl": "Telefoonnummer", 
    "en": "Phonenumber", 
    "sl": "", 
    "fi": ""
  };

  String get email => _email[languagePreference];
  Map<String, String> _email = <String, String>{
    "nl": "E-mail", 
    "en": "E-mail", 
    "sl": "", 
    "fi": ""
  };

  String get sendMessage => _sendMessage[languagePreference];
  Map<String, String> _sendMessage = <String, String>{
    "nl": "Bericht versturen", 
    "en": "Send message", 
    "sl": "", 
    "fi": ""
  };

  String get languageValue => _languageValue[languagePreference];
  Map<String, String> _languageValue = <String, String>{
    "nl": "Nederlands", 
    "en": "English", 
    "sl": "", 
    "fi": ""
  };

  String get overviewPage => _overviewPage[languagePreference];
  Map<String, String> _overviewPage = <String, String>{
    "nl": "Overzicht", 
    "en": "Overview", 
    "sl": "", 
    "fi": ""
  };

  String get expensesPage => _expensesPage[languagePreference];
  Map<String, String> _expensesPage = <String, String>{
    "nl": "Uitgaven", 
    "en": "Expenses", 
    "sl": "", 
    "fi": ""
  };

  String get insightsPage => _insightsPage[languagePreference];
  Map<String, String> _insightsPage = <String, String>{
    "nl": "Inzichten", 
    "en": "Insights", 
    "sl": "", 
    "fi": ""
  };

  String get settingsPage => _settingsPage[languagePreference];
  Map<String, String> _settingsPage = <String, String>{
    "nl": "Instellingen", 
    "en": "Settings", 
    "sl": "", 
    "fi": ""
  };


  
}
