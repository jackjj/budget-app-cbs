import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:sqflite/sqlite_api.dart';

class QuickSearch {
  static List<Map<String, dynamic>> tblQuickSearch;

  static Future<void> loadQuickSearchTable() async{
    Database db = await DatabaseHelper.instance.database;
    if (tblQuickSearch == null) {
      tblQuickSearch = await db.query(
        "tblQuickSearch",
      );
    }

  }

  static Future<List<String>> getShopSuggestions(bool withSpaced) async {
    await loadQuickSearchTable();

    List<String> results = [];
    for (String key in tblQuickSearch[0].keys) {
      if (withSpaced) {
        results.add(
          key.replaceAll("_", " "),
        );
      } else {
        results.add(key);
      }
    }
    return results;
  }

  static Future<List<String>> getProductSuggestions(String shopType) async {
    print(3287648478932749341);

    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> rawResults;

    List<String> shopNames = await getShopSuggestions(false);
    String columnName = shopType.replaceAll(" ", "_");
    List<String> results = [];

    if (shopNames.contains(columnName)) {
      rawResults = await db.query("tblQuickSearch", columns: [columnName]);
      for (Map<String, dynamic> row in rawResults) {
        if (row[columnName] != null) {
          results.add(row[columnName]);
        }
      }
    } else {
      rawResults = await db.query("tblQuickSearch", columns: ["Supermarkt"]);
      for (Map<String, dynamic> row in rawResults) {
        if (row["Supermarkt"] != null) {
          results.add(row["Supermarkt"]);
        }
      }
    }
    return results;
  }
}
