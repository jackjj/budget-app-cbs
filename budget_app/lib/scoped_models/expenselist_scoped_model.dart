

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ExpenseListModel extends Model{

  String searchWord;


  void reset(){
    searchWord = "";
    notifyListeners();
  }


  static ExpenseListModel of(BuildContext context) =>
      ScopedModel.of<ExpenseListModel>(context);

}