import 'dart:async';

import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:uuid/uuid.dart';

class NewTransactionModel extends Model {
  String store = "";
  String storeType = "";
  String date = DateFormat('yyyy-MM-dd').format(DateTime.now());
  bool abroadExpense = false;
  bool onlineExpense = false;
  String product = "";
  String productCategory = "";
  String price = "";
  double totalPrice = 0;
  List<Map<String, String>> products = List<Map<String, String>>();
  bool storeInfoComplete = false;
  bool productInfoComplete = false;
  int productCounter = 0;

  double discountPercentage = 0;
  double discountAmount = 0;
  String discountText = translate.enterDiscountReceipt + " ...";
  String itemMultiplier = "1";
  String receiptLocation = "";
  String receiptPrice = "";
  bool receiptComplete = false;

  // String addItemMultiplier;
  // String store;
  // String storeType;
  // String date;
  // bool abroadExpense;
  // bool onlineExpense;
  // String product;
  // String productCategory;
  // String price;
  // double discountPercentage;
  // double discountAmount;
  // String discountText;
  // double totalPrice;
  // List<Map<String, String>> products;
  // bool storeInfoComplete;
  // bool productInfoComplete;
  // int productCounter;

  String modifyTransactionID;

  void changeProductTotal(double valueChange) {
    totalPrice += valueChange * ((100 - discountPercentage) / 100);
    notifyListeners();
  }

  void changeProductDiscountAmount(double valueChange) {
    totalPrice = 0;

    products.forEach((productInfo) {
      totalPrice += double.parse(productInfo['price']);
    });
    discountAmount = valueChange;
    discountPercentage = 0;

    totalPrice = totalPrice - valueChange;
    notifyListeners();
  }

  void changeProductDiscountPercentage(double valueChange) {
    totalPrice = 0;
    products.forEach((productInfo) {
      totalPrice += double.parse(productInfo['price']);
    });
    totalPrice = totalPrice * ((100 - valueChange) / 100);
    discountAmount = 0;
    discountPercentage = valueChange;
    notifyListeners();
  }

  void addNewTransAction() {
    if (modifyTransactionID != null) {
      TransactionDatabase.deleteTransaction(modifyTransactionID);
    }  
    if (receiptLocation == "") {
      addManualTransaction();
    } else {
      addReceiptTransaction();
    }
  }

  void addReceiptTransaction() {
    String transactionID = Uuid().v1();
    TransactionDatabase trx = TransactionDatabase();
    trx.transactionID = transactionID;
    trx.store = store;
    trx.storeType = storeType;
    trx.date = date;
    trx.totalPrice = double.parse(receiptPrice).toStringAsFixed(2);
    trx.receiptLocation = receiptLocation;

    trx.discountAmount = "0";
    trx.discountPercentage = "0";
    trx.expenseAbroad = "false";
    trx.expenseOnline = "false";
    TransactionDatabase.insertTransaction(trx);

    // print(38934324);
    // print(transactionID);
    // print(store);
    // print(storeType);
    // print(date);
    // print(receiptPrice);
    // print(receiptLocation);
  }

  void addManualTransaction() {
    String transactionID = Uuid().v1();
    TransactionDatabase trx = TransactionDatabase();
    trx.transactionID = transactionID;
    trx.store = store;
    trx.storeType = storeType;
    trx.date = date;
    trx.discountAmount = discountAmount.toStringAsFixed(2);
    trx.discountPercentage = discountPercentage.toStringAsFixed(0);
    trx.expenseAbroad = abroadExpense ? "true" : "false";
    trx.expenseOnline = onlineExpense ? "true" : "false";
    trx.totalPrice = totalPrice.toStringAsFixed(2);
    trx.discountText = discountText;
    TransactionDatabase.insertTransaction(trx);

    products.forEach((productInfo) async {
      TransactionDatabase trx = TransactionDatabase();
      trx.transactionID = transactionID;
      trx.product = productInfo['product'];
      trx.productCategory = productInfo['productCategory'];
      trx.price = productInfo['price'];
      trx.productCode = await TransactionDatabase.getProductCode(productInfo['productCategory']);
      TransactionDatabase.insertProduct(trx);
    });
  }

  void isReceiptInfoComplete() {
    if (store != "" && receiptPrice != "") {
      receiptComplete = true;
    } else {
      receiptComplete = false;
    }
    notifyListeners();
  }

  void isStoreInfoComplete() {
    if (store != "") {
      storeInfoComplete = true;
    } else {
      storeInfoComplete = false;
    }
    notifyListeners();
  }

  void isProductInfoComplete() {
    isStoreInfoComplete();
    if (product != "" && productCategory != "" && price != "") {
      productInfoComplete = true;
    } else {
      productInfoComplete = false;
    }
    notifyListeners();
  }

  void addNewProduct() {
    int itemCount = int.parse(itemMultiplier);

    for (int i = 1; i <= itemCount; i++) {
      productCounter += 1;
      changeProductTotal(double.parse(price));
      products.add({
        "product": product,
        "productCategory": productCategory,
        "price": price,
        "id": Uuid().v1()
      });
    }

    itemMultiplier = "1";
    product = "";
    productCategory = "";
    price = "";
    productInfoComplete = false;
    notifyListeners();
  }

  void deleteProduct(Map<String, String> productInfo) {
    changeProductTotal(double.parse(productInfo['price']) * -1.0);
    productCounter -= 1;
    products.remove(productInfo);
    notifyListeners();
  }

  void editProduct(Map<String, String> productInfo) {
    price = productInfo['price'];
    product = productInfo['product'];
    productCategory = productInfo['productCategory'];
    productInfoComplete = true;
    notifyListeners();
  }

  Future modifyTransaction(String transactionID) async {
    reset();
    storeInfoComplete = true;
    productInfoComplete = true;
    receiptComplete = true;
    await TransactionDatabase.getTransactionInfo(transactionID, this);
    modifyTransactionID = transactionID;
    notifyListeners();
  }

  Future reset() async {
    store = "";
    storeType = "";
    date = DateFormat('yyyy-MM-dd').format(DateTime.now());
    abroadExpense = false;
    onlineExpense = false;
    product = "";
    productCategory = "";
    price = "";
    totalPrice = 0;
    products = List<Map<String, String>>();
    storeInfoComplete = false;
    productInfoComplete = false;
    productCounter = 0;

    discountPercentage = 0;
    discountAmount = 0;
    discountText = translate.enterDiscountReceipt + " ...";
    itemMultiplier = "1";

    receiptPrice = "";
    receiptLocation = "";
    receiptComplete = false;

    modifyTransactionID = null;
  }

  void addItemMultiplier() {
    int itemCount = int.parse(itemMultiplier);
    itemCount = itemCount + 1;
    itemMultiplier = itemCount.toString();
    notifyListeners();
  }

  void subtractItemMultiplier() {
    int itemCount = int.parse(itemMultiplier);
    if (itemCount > 1) {
      itemCount = itemCount - 1;
      itemMultiplier = itemCount.toString();
      notifyListeners();
    }
  }

  static NewTransactionModel of(BuildContext context) =>
      ScopedModel.of<NewTransactionModel>(context);
}
