import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:budget_onderzoek/util/color_pallet.dart';

final charts.Color myBlue = charts.Color(
    r: ColorPallet.lightBlue.red,
    g: ColorPallet.lightBlue.green,
    b: ColorPallet.lightBlue.blue,
    a: ColorPallet.lightBlue.alpha);

final charts.Color myGreen = charts.Color(
    r: ColorPallet.lightGreen.red,
    g: ColorPallet.lightGreen.green,
    b: ColorPallet.lightGreen.blue,
    a: ColorPallet.lightGreen.alpha);

final charts.Color myOrange = charts.Color(
    r: ColorPallet.orange.red,
    g: ColorPallet.orange.green,
    b: ColorPallet.orange.blue,
    a: ColorPallet.orange.alpha);

final charts.Color myPink = charts.Color(
    r: ColorPallet.pink.red,
    g: ColorPallet.pink.green,
    b: ColorPallet.pink.blue,
    a: ColorPallet.pink.alpha);

final charts.Color myDarkBlue = charts.Color(
    r: ColorPallet.darkBlue.red,
    g: ColorPallet.darkBlue.green,
    b: ColorPallet.darkBlue.blue,
    a: ColorPallet.darkBlue.alpha);

double x;
double y;
double f;

class DonutChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutChart.withSampleData() {
    return DonutChart(
      _createSampleData(),
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return charts.PieChart(
      seriesList,
      animate: animate,
      // Add an [ArcLabelDecorator] configured to render labels outside of the
      // arc with a leader line.
      //
      // Text style for inside / outside can be controlled independently by
      // setting [insideLabelStyleSpec] and [outsideLabelStyleSpec].
      //
      // Example configuring different styles for inside/outside:
      //       new charts.ArcLabelDecorator(
      //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
      //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
      defaultRenderer: charts.ArcRendererConfig(
        arcWidth: (30*x).floor(),
        arcRendererDecorators: [
          charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.outside),
        ],
      ),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<ExpenseCategory, String>> _createSampleData() {
    


    final data = [
      ExpenseCategory("leuke dingen", 80, myOrange),
      ExpenseCategory("persoonlijkle ontwikkeling", 29, myBlue),
      ExpenseCategory("dagelijkse boodschappen", 250, myGreen),
      ExpenseCategory("vaste lasten", 300, myPink),
    ];

    return [
      charts.Series<ExpenseCategory, String>(
        id: 'Category',
        domainFn: (ExpenseCategory category, _) => category.categoryName,
        measureFn: (ExpenseCategory category, _) => category.moneySpent,
        data: data,
        colorFn: (ExpenseCategory category, _) => category.color,
        labelAccessorFn: (ExpenseCategory category, _) =>
            '€${category.moneySpent}',
        outsideLabelStyleAccessorFn: (ExpenseCategory category, _) =>
            charts.TextStyleSpec(
                fontFamily: 'Source Sans Pro Bold',
                fontSize: (16*f).floor(), // size in Pts.
                color: myDarkBlue),
      )
    ];
  }
}

class ExpenseCategory {
  final String categoryName;
  final int moneySpent;
  final charts.Color color;

  ExpenseCategory(this.categoryName, this.moneySpent, this.color);
}
