import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:budget_onderzoek/util/user_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/util/custom_icons.dart';

import 'package:date_range_picker/date_range_picker.dart' as DateRangePicker;
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:toast/toast.dart';

double x;
double y;
double f;

class FliterDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 50.0 * y,
                  color: ColorPallet.lightBlue,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: 25.0 * x),
                      Text(
                        translate.selectFilter,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0 * f,
                            fontWeight: FontWeight.w600),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      InkWell(
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 25.0 * x,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(width: 25.0 * x),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                _DateFilterWidget(),
                _PriceRangeFilterWidget(),
              ],
            ),
          ),
          SizedBox(
            height: 15.0 * y,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                textColor: Colors.white,
                onPressed: () {
                  FilterModel.of(context).reset();
                },
                child: Text(
                  translate.removeFilters,
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 16 * f),
                ),
                color: ColorPallet.lightBlue,
              ),
            ],
          ),
          SizedBox(
            height: 25.0 * y,
          ),
        ],
      ),
    );
  }
}

class _DateFilterWidget extends StatelessWidget {
  Future<String> getStartDate() async {
    var dbStartValue = await TransactionDatabase.getFirstAndLastDate();
    return dbStartValue['first'];
  }

  Future<String> getEndDate() async {
    var dbEndValue = await TransactionDatabase.getFirstAndLastDate();
    return dbEndValue['last'];
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        Map<String, dynamic> dateRange =
            await TransactionDatabase.getFirstAndLastDate();
        final List<DateTime> picked = await DateRangePicker.showDatePicker(
            locale: Locale(translate.languagePreference),
            context: context,
            initialFirstDate: DateTime.parse(dateRange['first']),
            initialLastDate: DateTime.parse(dateRange['last']),
            firstDate: DateTime(2019),
            lastDate: DateTime(2020));
        if (picked != null && picked.length == 2) {
          FilterModel.of(context).startDate =
              DateFormat('yyyy-MM-dd').format(picked[0]);
          FilterModel.of(context).endDate =
              DateFormat('yyyy-MM-dd').format(picked[1]);
          FilterModel.of(context).notifyListeners();
        }
      },
      child: ScopedModelDescendant<FilterModel>(
        builder: (context, child, model) => ExpansionTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(translate.date,
                      style: TextStyle(
                          color: ColorPallet.darkBlue,
                          fontSize: 18.0 * f,
                          fontWeight: FontWeight.w600)),
                ],
              ),
              leading: Icon(Icons.calendar_today,
                  color: ColorPallet.darkBlue, size: 24.0 * x),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 16.0 * x, vertical: 16.0 * y),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(translate.beginDate,
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f)),
                      FutureBuilder(
                          future: getStartDate(),
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            if (!snapshot.hasData ||
                                snapshot.data.length == 0) {
                              return Container();
                            }
                            return Text(
                                DateFormat('EEEE, d MMMM',
                                        translate.languagePreference)
                                    .format(DateTime.parse(model.startDate != ""
                                        ? model.startDate
                                        : snapshot.data)),
                                style: TextStyle(
                                    color: model.startDate != snapshot.data &&
                                            model.startDate != ""
                                        ? ColorPallet.lightBlue
                                        : ColorPallet.midGray,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16 * f));
                          }),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 16.0 * x, vertical: 16.0 * y),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(translate.endDate,
                          style: TextStyle(
                              color: ColorPallet.darkBlue,
                              fontWeight: FontWeight.w600,
                              fontSize: 16 * f)),
                      FutureBuilder(
                          future: getEndDate(),
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            if (!snapshot.hasData ||
                                snapshot.data.length == 0) {
                              return Container();
                            }
                            return Text(
                                DateFormat('EEEE, d MMMM',
                                        translate.languagePreference)
                                    .format(DateTime.parse(model.endDate != ""
                                        ? model.endDate
                                        : snapshot.data)),
                                style: TextStyle(
                                    color: model.endDate != snapshot.data &&
                                            model.endDate != ""
                                        ? ColorPallet.lightBlue
                                        : ColorPallet.midGray,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16 * f));
                          }),
                    ],
                  ),
                ),
              ],
            ),
      ),
    );
  }
}

class TextEditingControllerWorkaroud extends TextEditingController {
  TextEditingControllerWorkaroud({String text}) : super(text: text);

  void setTextAndPosition(String newText, {int caretPosition}) {
    int offset = caretPosition != null ? caretPosition : newText.length;
    value = value.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: offset),
        composing: TextRange.empty);
  }
}

class _PriceRangeFilterWidget extends StatefulWidget {
  @override
  _PriceRangeFilterWidgetState createState() => _PriceRangeFilterWidgetState();
}

class _PriceRangeFilterWidgetState extends State<_PriceRangeFilterWidget> {
  TextEditingControllerWorkaroud minPriceController =
      TextEditingControllerWorkaroud();

  TextEditingControllerWorkaroud maxPriceController =
      TextEditingControllerWorkaroud();

  String minPrice = "";
  String maxPrice = "";

  Map<String, dynamic> hintText;

  void checkUserInput(String newPrice, BuildContext context, bool isMinPrice) {
    if (!UserInput.intValidator(newPrice)) {
      Toast.show(translate.invalidAmount, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);

      if (isMinPrice) {
        minPriceController.setTextAndPosition(minPrice);
      } else {
        maxPriceController.setTextAndPosition(maxPrice);
      }
    } else {
      if (isMinPrice) {
        minPrice = newPrice;
      } else {
        maxPrice = newPrice;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        minPriceController.text = "";
        maxPriceController.text = "";

        hintText = await TransactionDatabase.getLowestAndHighestValue();

        if (FilterModel.of(context).minimumPrice != "") {
          hintText['lowest'] = FilterModel.of(context).minimumPrice;
        }

        if (FilterModel.of(context).maximumPrice != "") {
          hintText['highest'] = FilterModel.of(context).maximumPrice;
        }

        minPrice = hintText['lowest'];
        maxPrice = hintText['highest'];

        await showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(
                contentPadding: EdgeInsets.all(0),
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        color: ColorPallet.lightBlue,
                        height: 68,
                        width: 400,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 20),
                            Padding(
                              padding: EdgeInsets.only(left: 35.0),
                              child: Text(translate.filterAmount,
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 20),
                        color: Colors.white,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 60,
                              width: 400,
                            ),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        "Min",
                                        style: TextStyle(
                                            color: ColorPallet.lightBlue,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 21),
                                      ),
                                      Container(
                                        width: 80,
                                        height: 40,
                                        child: TextField(
                                          textAlign: TextAlign.center,
                                          controller: minPriceController,
                                          style: TextStyle(
                                              color: ColorPallet.darkBlue,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                          autocorrect: false,
                                          keyboardType: TextInputType.number,
                                          onChanged: (newPrice) {
                                            setState(() {
                                              checkUserInput(
                                                  newPrice, context, true);
                                            });
                                          },
                                          onSubmitted: (value) {},
                                          decoration: InputDecoration(
                                            hintText: hintText['lowest'],
                                            hintStyle: TextStyle(
                                                color: ColorPallet.midGray,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                            filled: true,
                                            fillColor: Colors.transparent,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(width: 80),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        "Max",
                                        style: TextStyle(
                                            color: ColorPallet.lightBlue,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 21),
                                      ),
                                      Container(
                                        width: 80,
                                        height: 40,
                                        child: TextField(
                                          textAlign: TextAlign.center,
                                          controller: maxPriceController,
                                          style: TextStyle(
                                              color: ColorPallet.darkBlue,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18),
                                          autocorrect: false,
                                          keyboardType: TextInputType.number,
                                          onChanged: (newPrice) {
                                            setState(() {
                                              checkUserInput(
                                                  newPrice, context, false);
                                            });
                                          },
                                          onSubmitted: (value) {},
                                          decoration: InputDecoration(
                                            hintText: hintText['highest'],
                                            hintStyle: TextStyle(
                                                color: ColorPallet.midGray,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                            filled: true,
                                            fillColor: Colors.transparent,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ]),
                            SizedBox(height: 60),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      translate.cancel.toUpperCase(),
                                      style: TextStyle(
                                          color: ColorPallet.lightBlue),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(
                                      translate.ok.toUpperCase(),
                                      style: TextStyle(
                                          color: ColorPallet.lightBlue),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                      FilterModel.of(context).minimumPrice =
                                          minPrice;
                                      FilterModel.of(context).maximumPrice =
                                          maxPrice;
                                      FilterModel.of(context).notifyListeners();
                                    },
                                  )
                                ]),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              );
            });
      },
      child: ScopedModelDescendant<FilterModel>(
        builder: (context, child, model) => ExpansionTile(
              title: Text(translate.priceRange,
                  style: TextStyle(
                      color: ColorPallet.darkBlue,
                      fontSize: 18.0 * f,
                      fontWeight: FontWeight.w600)),
              leading: Icon(Icons.euro_symbol,
                  color: ColorPallet.darkBlue, size: 24.0 * x),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 16.0 * x, vertical: 16.0 * y),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        translate.minimum,
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                            fontSize: 16 * f),
                      ),
                      FutureBuilder<String>(
                        future: getMinimumValue(),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> snapshot) {
                          if (!snapshot.hasData || snapshot.data.length == 0) {
                            return Container();
                          }
                          return Text(
                            model.minimumPrice != ""
                                ? model.minimumPrice
                                : snapshot.data,
                            style: TextStyle(
                                color: model.minimumPrice != snapshot.data &&
                                        model.minimumPrice != ""
                                    ? ColorPallet.lightBlue
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w600,
                                fontSize: 16 * f),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 16.0 * x, vertical: 16.0 * y),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        translate.maximum,
                        style: TextStyle(
                            color: ColorPallet.darkBlue,
                            fontWeight: FontWeight.w600,
                            fontSize: 16 * f),
                      ),
                      FutureBuilder<String>(
                        future: getMaximumValue(),
                        builder: (BuildContext context,
                            AsyncSnapshot<String> snapshot) {
                          if (!snapshot.hasData || snapshot.data.length == 0) {
                            return Container();
                          }
                          return Text(
                            model.maximumPrice != ""
                                ? model.maximumPrice
                                : snapshot.data,
                            style: TextStyle(
                                color: model.maximumPrice != snapshot.data &&
                                        model.maximumPrice != ""
                                    ? ColorPallet.lightBlue
                                    : ColorPallet.midGray,
                                fontWeight: FontWeight.w600,
                                fontSize: 16 * f),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
      ),
    );
  }

  Future<String> getMinimumValue() async {
    var dbMinValue = await TransactionDatabase.getLowestAndHighestValue();
    return dbMinValue['lowest'];
  }

  Future<String> getMaximumValue() async {
    var dbHighValue = await TransactionDatabase.getLowestAndHighestValue();
    return dbHighValue['highest'];
  }
}

class _FixedOrVariableFilterWidget extends StatelessWidget {
  const _FixedOrVariableFilterWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(translate.fixedOrVariable,
          style: TextStyle(
              color: ColorPallet.darkBlue,
              fontSize: 18.0 * f,
              fontWeight: FontWeight.w600)),
      leading: Icon(CustomIcons.variable_or_fixed,
          color: ColorPallet.darkBlue, size: 24.0 * x),
      children: <Widget>[
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Type uitgaven",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Text(
                "Beide",
                style: TextStyle(
                    color: ColorPallet.midGray,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _CategoriesFilterWidget extends StatelessWidget {
  const _CategoriesFilterWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(translate.categories,
          style: TextStyle(
              color: ColorPallet.darkBlue,
              fontSize: 18.0 * f,
              fontWeight: FontWeight.w600)),
      leading:
          Icon(Icons.category, color: ColorPallet.darkBlue, size: 24.0 * x),
      children: <Widget>[
        Padding(
          padding:
              EdgeInsets.only(top: 16.0 * y, left: 16.0 * x, right: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                translate.everything,
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * y),
              ),
              Radio(
                value: 0,
                onChanged: (val) {},
                groupValue: 0,
                activeColor: ColorPallet.lightBlue,
              ),
            ],
          ),
        ),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
          child: Text(
            "Vaste lasten",
            style: TextStyle(
                color: ColorPallet.darkBlue,
                fontWeight: FontWeight.w700,
                fontSize: 16 * f),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Woonkosten",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Verzekeringen\nen belastingen",
                  style: TextStyle(
                      color: ColorPallet.darkBlue,
                      fontWeight: FontWeight.w600,
                      fontSize: 16 * f)),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Abbonementen",
                  style: TextStyle(
                      color: ColorPallet.darkBlue,
                      fontWeight: FontWeight.w600,
                      fontSize: 16 * f)),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Overige",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: 16.0 * x, vertical: 16.0 * y),
          child: Text(
            "Variabele uitgaven",
            style: TextStyle(
                color: ColorPallet.darkBlue,
                fontWeight: FontWeight.w700,
                fontSize: 16 * f),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Overige",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Giften & Donaties",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Shoppen",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Vrije tijd en sociaal",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Educatie",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Werk en investeren",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Fitness en verzorging",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Gezondheid",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Familie en huisdieren",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Restaurant",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Vervoer",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Boodschappen",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
      ],
    );
  }
}

class _PersonFilterWidget extends StatelessWidget {
  const _PersonFilterWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(translate.people,
              style: TextStyle(
                  color: ColorPallet.darkBlue,
                  fontSize: 18.0 * f,
                  fontWeight: FontWeight.w600)),
          SizedBox(
            height: 4.0 * y,
          ),
          Text("Tom",
              style: TextStyle(
                  color: ColorPallet.lightBlue,
                  fontSize: 13.0 * f,
                  fontWeight: FontWeight.w600)),
        ],
      ),
      leading: Icon(Icons.group, color: ColorPallet.darkBlue, size: 24.0 * x),
      children: <Widget>[
        Padding(
          padding:
              EdgeInsets.only(top: 16.0 * y, left: 16.0 * x, right: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Tom",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(
                value: 0,
                onChanged: (val) {},
                groupValue: 0,
                activeColor: ColorPallet.lightBlue,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Kim",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Dexter",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Karin",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * f),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0 * x),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Ron",
                style: TextStyle(
                    color: ColorPallet.darkBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 16 * x),
              ),
              Radio(value: true, onChanged: (val) {}, groupValue: 0),
            ],
          ),
        ),
      ],
    );
  }
}
